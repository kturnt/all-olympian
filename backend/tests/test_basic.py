import sys
import os
import unittest

sys.path.append("..")
from application import app


class BasicTests(unittest.TestCase):
    def setUp(self) -> None:
        self.app = app.test_client()

    def test_main(self):
        response = self.app.get("/", follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data, b"<h1>All Olympian API</h1>")


if __name__ == "__main__":
    unittest.main()
