import sys
import json
import unittest

sys.path.append("..")
from application import app


class TestCountries(unittest.TestCase):
    def setUp(self) -> None:
        self.app = app.test_client()

    def test_all_countries(self):
        response = self.app.get("/api/countries", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

    def test_country_by_code(self):
        response = self.app.get("/api/countries/usa", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

        usa = json.loads(response.data)
        self.assertEqual(usa["name"], "united_states")

    def test_countries_athletes(self):
        countries = ["usa", "rus", "chn"]
        for country in countries:
            response = self.app.get(
                "/api/countries/{}/athletes".format(country), follow_redirects=True
            )
            self.assertEqual(response.status_code, 200)

            athletes = json.loads(json.loads(response.data))
            self.assertTrue(type(athletes) is dict)
            self.assertGreater(len(athletes), 0)


if __name__ == "__main__":
    unittest.main()
