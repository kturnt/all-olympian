import sys
import json
import unittest

sys.path.append("..")
from application import app


class TestEvents(unittest.TestCase):
    def setUp(self) -> None:
        self.app = app.test_client()

    def test_all_events(self):
        response = self.app.get("/api/events", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

        events = json.loads(response.data)
        self.assertGreater(len(events), 0)

    def test_season_events(self):
        response = self.app.get("/api/events?season=winter", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

        events = json.loads(response.data)
        self.assertTrue(all(event["season"] == "winter" for event in events))

    def test_sport_events(self):
        response = self.app.get("/api/events?sport=swimming", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

        events = json.loads(response.data)
        self.assertTrue(
            all(event["overarching_sport"] == "Swimming" for event in events)
        )

    def test_event_by_id(self):
        response = self.app.get(
            "/api/event?id=2754fca4-fa06-4f44-9296-60434084a403", follow_redirects=True
        )
        self.assertEqual(response.status_code, 200)

        event = json.loads(response.data)
        self.assertEqual("Men's Individual", event["event"])


if __name__ == "__main__":
    unittest.main()
