import sys
import json
import unittest

sys.path.append("..")
from application import app


class TestSearch(unittest.TestCase):
    def setUp(self) -> None:
        self.app = app.test_client()

    def testSearchTermReturn(self):
        # ensure search terms are returned space separated
        response = self.app.get("/api/search?query=some%20words")

        self.assertEqual(response.status_code, 200)

        response_data = json.loads(response.data)
        self.assertTrue("words" in response_data)
        self.assertEqual(response_data["words"], ["some", "words"])

    def testSearchUSA(self):
        response = self.app.get("/api/search?query=usa")

        self.assertEqual(response.status_code, 200)

        response_data = json.loads(response.data)
        self.assertTrue(
            any(
                "United States" in country["display_name"]
                for country in response_data["countries"]
            )
        )
