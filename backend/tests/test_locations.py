import sys
import json
import unittest

sys.path.append("..")
from application import app


class TestLocations(unittest.TestCase):
    def setUp(self) -> None:
        self.app = app.test_client()

    def test_all_locations(self):
        response = self.app.get("/api/olympics", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

        locations = json.loads(response.data)
        self.assertGreater(len(locations), 0)

    def test_location_by_id(self):
        response = self.app.get(
            "/api/olympics?id=0060202f-194f-47f9-9445-e5a96982455a",
            follow_redirects=True,
        )
        self.assertEqual(response.status_code, 200)

        location = json.loads(response.data)[0]
        self.assertEqual(location["city"], "London")

    def test_location_by_ids(self):
        response = self.app.get(
            "/api/olympics?ids=0b07b880-2aa1-46e0-a906-549391b4dd17,0060202f-194f-47f9-9445-e5a96982455a",
            follow_redirects=True,
        )

        self.assertEqual(response.status_code, 200)

        locations = json.loads(response.data)
        self.assertEqual(len(locations), 2)

        location_names = set(["London", "Beijing"])
        result_location_names = set([location["city"] for location in locations])
        self.assertEqual(location_names, result_location_names)

    def test_location_by_city(self):
        response = self.app.get("/api/olympics?city=rome", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

        location = json.loads(response.data)[0]
        self.assertEqual(location["hosting_country"], "Italy")

    def test_location_by_year(self):
        response = self.app.get("/api/olympics?year=2016", follow_redirects=True)
        self.assertEqual(response.status_code, 200)

        location = json.loads(response.data)[0]
        self.assertEqual(location["city"], "Rio")


if __name__ == "__main__":
    unittest.main()
