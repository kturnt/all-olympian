from database import Base


class Event(Base):
    """"""

    __tablename__ = "events"
    __table_args__ = {"autoload": True}

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Country(Base):
    __tablename__ = "countries"
    __table_args__ = {"autoload": True}

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}


class Location(Base):
    __tablename__ = "locations"
    __table_args__ = {"autoload": True}

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}
