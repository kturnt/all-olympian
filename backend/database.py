from sqlalchemy import create_engine
from sqlalchemy.sql.expression import or_
from sqlalchemy.sql.sqltypes import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

engine = create_engine(
    "postgres://postgres:nXbeYABGhFssJ7Yzv3TGAqR9@allolympian.ckswqi2ltby3.us-west-2.rds.amazonaws.com:5432/all_olympian",
    echo=True,
)
Base = declarative_base(engine)


Session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
session = Session()


def db_search(model, search_strs, excluding_cols=()):
    query = session.query(model)

    conditions = []
    for col in model.__table__.columns:
        if col.name in excluding_cols:
            continue

        for search_str in search_strs:

            if col.type.__class__ is VARCHAR:
                conditions.append(
                    getattr(model, col.name).ilike("%{}%".format(search_str))
                )
            elif col.type.__class__ is INTEGER and search_str.isdigit():
                conditions.append(getattr(model, col.name).__eq__(int(search_str)))

    conditions = or_(*conditions)

    return query.filter(conditions).all()


if __name__ == "__main__":
    # scripts for populating the database
    from models import *
    import csv
    import json
    import os
    import uuid

    """
    # events
    events_file = os.path.expanduser('~/Downloads/data-aquisition_final-data_events.json')

    with open(events_file, 'r') as f:
        events_data = json.loads(f.read())

    for event in events_data:

        event_obj = Event(event=event['event'],
                          gender=event['gender'],
                          id=event['id'],
                          image_url=event['image_url'],
                          season=event['season'],
                          overarching_sport=event['overarching_sport'],
                          latest_champion=event['latest_champion'],
                          latest_champion_country=event['latest_champion_country'],
                          top_athlete=event['top_athlete'],
                          top_athlete_country=event['top_athlete_country'],
                          debut_location=event['debut_location'] if event['debut_location'] != 'N/A' else None,
                          debut_year=event['debut_year'] if event['debut_year'] != 'N/A' else None)

        session.add(event_obj)

    session.commit()
    """

    # locations images
    """
    locations_images_file = os.path.expanduser('~/Downloads/data-aquisition_location_images.json')

    with open(locations_images_file, 'r') as f:
        location_images = json.loads(f.read())

    location_images_map = dict()
    for location_image_obj in location_images:
        location_images_map[location_image_obj['location'] + location_image_obj['year']] = (location_image_obj['logo'],
                                                                                            location_image_obj['opening ceremony'])

    all_locations = session.query(Location).all()
    for location in all_locations:
        images_for_location = location_images_map[location.city + str(location.year)]
        location.logo_url = images_for_location[0]
        location.opening_ceremony_url = images_for_location[1]

    session.commit()
    """

    # locations
    locations_file = os.path.expanduser(
        "~/Downloads/data-aquisition_final-data_locations.json"
    )

    with open(locations_file, "r") as f:
        locations_data = json.loads(f.read())

    for location in locations_data:
        location_obj = Location(
            city=location["city"],
            hosting_country=location["hosting_country"],
            id=location["id"],
            logo_url=location["logo_url"],
            num_athletes=location["num_athletes"],
            num_events=location["num_events"],
            num_participating_countries=location["num_participating_countries"],
            opening_ceremony_url=location["opening_ceremony_url"],
            season=location["season"],
            top_athlete=location["top_athlete"],
            top_athlete_country=location["top_athlete_country"],
            year=location["year"],
            all_events=location["all_events"],
            host_code=location["host_code"],
        )
        session.add(location_obj)
    session.commit()

    # countries images
    """
    countries_images_file = os.path.expanduser('~/Downloads/data-aquisition_countries.json')

    with open(countries_images_file, 'r') as f:
        countries_images = json.loads(f.read())

    for country in countries_images:
        country_row = session.query(Country).filter_by(ioc_code=country['code']).first()
        if not country_row:
            print('could not find: {}'.format(country['code']))
            assert False

        country_row.capital_image_url = country['capital image']
        country_row.olympics_image_url = country['olympics image']

    session.commit()
    """
    """
    # countries
    countries_file = os.path.expanduser('~/Downloads/data-aquisition_final-data_countries.json')

    with open(countries_file, 'r') as f:
        countries_data = f.read()
        countries = json.loads(countries_data)

    for country in countries:
        # noinspection PyArgumentList

        # assemble country row
        country_row = Country(name=country['name'],
                              display_name=country['display_name'],
                              ioc_code=country['code'],
                              capital=country['capital'],
                              region=country['region'],
                              country_flag_url=country['flag'],
                              population=country['population'],
                              gdp=country['gdp'],
                              medal_count=country['medal_count'],
                              athletes=json.dumps(country['athletes']),
                              medals=json.dumps(country['medals']),
                              locations_participated=country['attended'],
                              languages=country['languages'],
                              hosted=country['hosted'],
                              capital_image_url=country['capital_image'],
                              olympics_image_url=country['olympics_image'],
                              top_athl_in=country['top_athl_in'],
                              latest_champ_in=country['latest_champ_in'],
                              all_events=country['all_events'],
                              medaled_events=country['medaled_events'])
        session.add(country_row)
    session.commit()
    """
