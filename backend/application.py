import json
from exceptions import *
from flask import Flask
from flask import request, jsonify
from flask_cors import CORS

from models import *
from database import session, db_search

app = Flask(__name__)
CORS(app)


@app.route("/")
def hello():
    return "<h1>All Olympian API</h1>"


@app.route("/api/events")
def events():
    sport = request.args.get("sport")
    season = request.args.get("season")
    ids = request.args.get("ids")

    if ids:
        ids = ids.split(",")
        event_objs = session.query(Event).filter(Event.id.in_(ids))
        return jsonify([event_obj.as_dict() for event_obj in event_objs])
    else:
        params = dict()
        if season is not None:
            if season == "winter" or season == "summer":
                params["season"] = season
            else:
                raise InvalidArgs(
                    "Must specify either 'summer' or 'winter' for 'season' parameter"
                )
        if sport is not None:
            params["overarching_sport"] = sport.capitalize()

        events_for_season = session.query(Event).filter_by(**params).all()

        event_data = []
        for event in events_for_season:
            event_data.append(event.as_dict())

    return jsonify(event_data)


@app.route("/api/event")
def event():
    # filter by id or name
    id = request.args.get("id")
    name = request.args.get("name")

    if id is not None:
        event_obj = session.query(Event).filter_by(id=id).first()
    elif name is not None:
        event_obj = session.query(Event).filter_by(event=name).first()
    else:
        raise InvalidArgs("Must specify either 'name' or 'id' of an event")

    if event_obj is None:
        return jsonify({})

    return jsonify(event_obj.as_dict())


@app.route("/api/olympics")
def olympics():
    ids = request.args.get("ids")
    city = request.args.get("city")
    year = request.args.get("year")

    if ids:
        ids = ids.split(",")
        locations = session.query(Location).filter(Location.id.in_(ids)).all()
    else:
        if city is not None and year is not None:
            locations = (
                session.query(Location)
                .filter_by(city=city.capitalize(), year=year)
                .all()
            )
        elif city is None and year is not None:
            locations = session.query(Location).filter_by(year=year).all()
        elif city is not None and year is None:
            locations = session.query(Location).filter_by(city=city.capitalize()).all()
        else:
            locations = session.query(Location).all()

    location_data = [location.as_dict() for location in locations]
    return jsonify(location_data)


@app.route("/api/countries/")
def all_countries():
    countries_data = session.query(Country).all()
    return jsonify([country.as_dict() for country in countries_data])


@app.route("/api/countries/<abbr>")
def countries(abbr):
    country_for_abbr = session.query(Country).filter_by(ioc_code=abbr.upper()).first()
    return jsonify(country_for_abbr.as_dict())


@app.route("/api/countries/<abbr>/athletes")
def athletes_by_country(abbr):
    country_for_abbr = session.query(Country).filter_by(ioc_code=abbr.upper()).first()
    return jsonify(country_for_abbr.athletes)


@app.route("/api/search")
def search():
    search_query = request.args.get("query")
    if search_query is None:
        raise InvalidArgs("Must specify a search query")

    search_components = search_query.split()  # split into individual words

    search_result = {"words": search_components}

    # search locations
    exclude = ("opening_ceremony_url", "logo_url", "id")
    location_results = db_search(Location, search_components, exclude)
    search_result["locations"] = [location.as_dict() for location in location_results]

    # search countries
    exclude = (
        "olympics_image_url",
        "country_flag_url",
        "capital_image_url",
        "id",
        "population",
        "gdp",
    )
    country_results = db_search(Country, search_components, exclude)
    search_result["countries"] = [country.as_dict() for country in country_results]

    # search events
    exclude = ("image_url", "id")
    event_results = db_search(Event, search_components, exclude)
    search_result["events"] = [event.as_dict() for event in event_results]

    return jsonify(search_result)


# error handling
@app.errorhandler(InvalidArgs)
def handle_invalid_args(error):
    response = jsonify(error.to_dict())
    response.status_code = error.status_code
    return response


# tear down
@app.teardown_appcontext
def shutdown_session(exception=None):
    session.close()


# run the app.
if __name__ == "__main__":
    # Setting debug to True enables debug output. This line should be
    # removed before deploying a production app.
    app.debug = True
    app.run(host="0.0.0.0")
