import React from 'react';
import ReactDOM from 'react-dom';
import App from '../src/App.js'
import AllLocations from '../src/pages/AllLocations.js';
import AllEvents from '../src/pages/AllEvents.js';
import AllCountries from '../src/pages/AllCountries';
import Event from '../src/pages/Event.js';
import Location from '../src/pages/Location.js';
import Country from '../src/pages/Country.js';


describe('AllEvents Component', function() {
    it('Renders', function() {
        sinon.spy(AllEvents.prototype, 'componentDidMount');
        const wrapper = mount(<AllEvents />);
        expect(AllEvents.prototype.componentDidMount).to.have.property('callCount', 1);
        wrapper.unmount();
      });  

    it('AllEvents', function() {
        const wrapper = shallow(<AllEvents />);
        expect(wrapper.text().length).to.be.gt(0);
      });  
});  


describe('AllCountries', function () {
    it('Renders', function() {
        sinon.spy(AllCountries.prototype, 'componentDidMount');
        const wrapper = mount(<AllCountries />);
        expect(AllCountries.prototype.componentDidMount).to.have.property('callCount', 1);
        wrapper.unmount();
    });  
      
    it('AllCountries Text', () => {
        const wrapper = shallow(<AllCountries />);
        expect(wrapper.text().length).to.be.gt(0);
    });
});


describe('AllLocations', function () {
    it('Renders', function() {
        sinon.spy(AllLocations.prototype, 'componentDidMount');
        const wrapper = mount(<AllLocations />);
        expect(AllLocations.prototype.componentDidMount).to.have.property('callCount', 1);
        wrapper.unmount();
    });  
      
    it('AllLocations Text', () => {
        const wrapper = shallow(<AllLocations />);
        expect(wrapper.text().length).to.be.gt(0);
    });
});


describe('Event', function () {
    it('Renders', function() {
        sinon.spy(Event.prototype, 'componentDidMount');
        const wrapper = mount(<Event />);
        expect(Event.prototype.componentDidMount).to.have.property('callCount', 1);
        wrapper.unmount();
    });  
      
    it('Event Text', () => {
        const wrapper = shallow(<Event />);
        expect(wrapper.text().length).to.be.gt(0);
    });
});


describe('Location', function () {
    it('Renders', function() {
        sinon.spy(Location.prototype, 'componentDidMount');
        const wrapper = mount(<Location />);
        expect(Location.prototype.componentDidMount).to.have.property('callCount', 1);
        wrapper.unmount();
    });  
      
    it('Location Text', () => {
        const wrapper = shallow(<Location />);
        expect(wrapper.text().length).to.be.gt(0);
    });
});

describe('Country', function () {
    it('Renders', function() {
        sinon.spy(Country.prototype, 'componentDidMount');
        const wrapper = mount(<Country />);
        expect(Country.prototype.componentDidMount).to.have.property('callCount', 1);
        wrapper.unmount();
    });  
      
    it('Country Text', () => {
        const wrapper = shallow(<Country />);
        expect(wrapper.text().length).to.be.gt(0);
    });
});