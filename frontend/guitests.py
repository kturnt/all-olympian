from unittest import main, TestCase
from selenium import webdriver
import chromedriver_binary
from selenium.webdriver.support.ui import Select
import time

link = "https://development.all-olympian.com"

class GUI_Tests(TestCase):
    @classmethod
    def setUpClass(cls):
        # create a new Chrome session
        cls.driver = webdriver.Chrome()
        # navigation to the application home page
        cls.driver.get(link)


    def test_about_page(self):
        # grab the home page
        self.driver.get(link)
        # about page text on nav bar
        self.driver.find_element_by_link_text("About").click()
        time.sleep(2)
        self.assertEqual(self.driver.current_url, link + "/about")

    def test_splash_page(self):
        # clicking on the All Olympian text should take user to home page
        self.driver.find_element_by_class_name("navbar-brand").click()
        time.sleep(2)
        self.assertEqual(self.driver.current_url, link + "/")

    def test_all_events_page(self):
        self.driver.find_element_by_link_text("Events").click()
        time.sleep(2)
        self.assertEqual(self.driver.current_url, link + "/events")
        # make sure that the number of events per row is 3
        self.assertEqual(len(self.driver.find_elements_by_class_name("card-deck")), 3)
        # test last page pagination for events (last page button)
        self.driver.find_element_by_xpath("/html/body/div/div/main/main/section[3]/div/ul/nav/ul/li[7]").click()
        time.sleep(2)
        # test pagination and see that size of table changed on final page (should be 1)
        self.assertEqual(len(self.driver.find_elements_by_class_name("card-deck")), 2)
    
    def test_all_countries_page(self):
        self.driver.find_element_by_link_text("Countries").click()
        time.sleep(2)
        self.assertEqual(self.driver.current_url, link + "/countries")
        # find size of table (should be 10)
        self.assertEqual(len(self.driver.find_elements_by_xpath("/html/body/div/div/main/main/div/table/tbody/tr")), 10)
        # test pagination for countries (last page button)
        self.driver.find_element_by_xpath("/html/body/div/div/main/main/section[3]/div/ul/nav/ul/li[7]").click()
        time.sleep(2)
        # test pagination and see that size of table changed on final page (should be 1)
        self.assertEqual(len(self.driver.find_elements_by_xpath("/html/body/div/div/main/main/div/table/tbody/tr")), 1)
        
    def test_all_locations_page(self):
        # locations page uses the same css as events page
        self.driver.find_element_by_link_text("Locations").click()
        time.sleep(2)
        self.assertEqual(self.driver.current_url, link + "/locations")
        # make sure that the number of locations per row is 3
        self.assertEqual(len(self.driver.find_elements_by_class_name("card-deck")), 3)
        # test last page pagination for locations
        self.driver.find_element_by_xpath("/html/body/div/div/main/main/section[3]/div/ul/nav/ul/li[7]").click()
        time.sleep(2)
        # test pagination and see that size of table changed on final page (should be 1)
        self.assertEqual(len(self.driver.find_elements_by_class_name("card-deck")), 2)

    def test_event_page(self):
        # AllEvents page
        self.driver.find_element_by_link_text("Events").click()
        time.sleep(2)
        self.assertEqual(self.driver.current_url, link + "/events")
        # single instance of an event page
        self.driver.find_element_by_class_name('card_link').click()
        time.sleep(2)
        # checking table entries in the single event's table
        self.assertTrue("Sport" in self.driver.page_source)
        self.assertTrue("Gender" in self.driver.page_source)
        self.assertTrue("Season" in self.driver.page_source)
        self.assertTrue("Latest Winner" in self.driver.page_source)
        self.assertTrue("Top Athlete" in self.driver.page_source)

    def test_country_page(self):
        # AllCountries page
        self.driver.find_element_by_link_text("Countries").click()
        time.sleep(2)
        self.assertEqual(self.driver.current_url, link + "/countries")
        # click first country link on table
        self.driver.find_element_by_xpath("/html/body/div/div/main/main/div/table/tbody/tr[1]/td[1]/a").click()
        time.sleep(2)
        # checking table entries in the single country's table
        self.assertTrue("IOC Code" in self.driver.page_source)
        self.assertTrue("Languages" in self.driver.page_source)
        self.assertTrue("Capital" in self.driver.page_source)
        self.assertTrue("Region" in self.driver.page_source)
        self.assertTrue("Population" in self.driver.page_source)
        self.assertTrue("GDP" in self.driver.page_source)
        self.assertTrue("Olympic Games Participated In" in self.driver.page_source)
        self.assertTrue("Medal Count" in self.driver.page_source)

    def test_location_page(self):
        # AllLocations page
        self.driver.find_element_by_link_text("Locations").click()
        time.sleep(2)
        self.assertEqual(self.driver.current_url, link + "/locations")
        # single instance of an location page
        # we used the same css for locations as events
        self.driver.find_element_by_class_name('card_link').click()
        time.sleep(2)
        # checking table entries in the single location's table
        self.assertTrue("City" in self.driver.page_source)
        self.assertTrue("Hosting Country" in self.driver.page_source)
        self.assertTrue("Season" in self.driver.page_source)
        self.assertTrue("Number of Athletes" in self.driver.page_source)
        self.assertTrue("Number of Events" in self.driver.page_source)
        self.assertTrue("Number of Participating Countries" in self.driver.page_source)
        self.assertTrue("Top Athlete" in self.driver.page_source)

    # phase 3 - refined previous tests and added tests below

    def test_search_bar(self):
        # Splash page
        self.driver.find_element_by_class_name("navbar-brand").click()
        time.sleep(3)
        # Website wide search
        self.driver.find_element_by_class_name("form-control").send_keys("USA")
        time.sleep(2)
        # Go button for search
        self.driver.find_element_by_xpath('//*[@id="root"]/div/div/nav/form/div/div').click()
        time.sleep(2)
        # Assert we get United States as a return from the search
        self.assertEqual(self.driver.find_element_by_xpath('//*[@id="root"]/div/main/main/table[1]/tbody/tr[1]/td[1]/a/div').text, "United States")

    def test_events_search_bar(self):
        # Go to Events page
        self.driver.find_element_by_link_text("Events").click()
        time.sleep(3)
        self.assertEqual(self.driver.current_url, link + "/events")
        # Fill in search bar
        self.driver.find_element_by_id("localSearch").send_keys("basketball")
        time.sleep(2)
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/div[1]/div/button').click()
        time.sleep(2)
        self.assertEqual(len(self.driver.find_elements_by_class_name("card-deck")), 1)
        self.assertEqual(self.driver.find_element_by_xpath('/html/body/div/div/main/main/section[2]/div[1]/div[1]/div/div').text, "Men's Basketball")

    def test_events_filter_sort(self):
        # Go to Events page
        self.driver.find_element_by_link_text("Events").click()
        time.sleep(3)
        self.assertEqual(self.driver.current_url, link + "/events")
        # Filter drop down
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/div[2]/div[2]/button').click()
        time.sleep(2)
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/div[2]/div[2]/div/button[2]').click()
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/div[2]/button').click()
        time.sleep(2)
        # Confirm first card is correct
        self.assertEqual(self.driver.find_element_by_xpath('/html/body/div/div/main/main/section[2]/div[1]/div[1]/div/div').text, "Women's BMX")
        
        # Sort drop down
        # Desc.
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/div[3]/div[2]/button').click()
        time.sleep(3)
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/div[3]/div[2]/div/button[2]').click()
        # Season
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/div[3]/div[3]/button').click()
        time.sleep(3)
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/div[3]/div[3]/div/button[4]').click()
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/div[3]/button').click()
        time.sleep(3)
        # Confirm first card is correct
        self.assertEqual(self.driver.find_element_by_xpath('/html/body/div/div/main/main/section[2]/div[1]/div[1]/div/div').text, "Women's 10 kilometers")

        # Clear All
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/button').click()
        time.sleep(3)
        # Confirm first card is correct
        self.assertEqual(self.driver.find_element_by_xpath('/html/body/div/div/main/main/section[2]/div[1]/div[1]/div/div').text, "Men's 1,500 meters")

    def test_locations_search_bar(self):
        # Go to Locations page
        self.driver.find_element_by_link_text("Locations").click()
        time.sleep(3)
        self.assertEqual(self.driver.current_url, link + "/locations")
        # Fill in search bar
        self.driver.find_element_by_id("localSearch").send_keys("Rio")
        time.sleep(2)
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/div[1]/div/button').click()
        time.sleep(2)
        self.assertEqual(len(self.driver.find_elements_by_class_name("card-deck")), 1)
        self.assertEqual(self.driver.find_element_by_xpath('/html/body/div/div/main/main/section[2]/div[1]/div[1]/div/div').text, "Rio 2016")

    def test_locations_filter_sort(self):
        # Go to Locations page
        self.driver.find_element_by_link_text("Locations").click()
        time.sleep(3)
        self.assertEqual(self.driver.current_url, link + "/locations")
        # Filter drop down
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/div[2]/div[2]/button').click()
        time.sleep(2)
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/div[2]/div[2]/div/button[2]').click()
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/div[2]/button').click()
        time.sleep(2)
        # Confirm first card is correct
        self.assertEqual(self.driver.find_element_by_xpath('/html/body/div/div/main/main/section[2]/div[1]/div[1]/div/div').text, "Lake Placid 1980")
        
        # Sort drop down
        # Desc.
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/div[3]/div[2]/button').click()
        time.sleep(3)
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/div[3]/div[2]/div/button[2]').click()
        # Sort
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/div[3]/div[3]/button').click()
        time.sleep(3)
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/div[3]/div[3]/div/button[4]').click()
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/div[3]/button').click()
        time.sleep(3)
        # Confirm first card is correct
        self.assertEqual(self.driver.find_element_by_xpath('/html/body/div/div/main/main/section[2]/div[1]/div[1]/div/div').text, "Chamonix 1924")

        # Clear All
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/button').click()
        time.sleep(3)
        # Confirm first card is correct
        self.assertEqual(self.driver.find_element_by_xpath('/html/body/div/div/main/main/section[2]/div[1]/div[1]/div/div').text, "London 2012")

    def test_countries_search_bar(self):
        # Go to Countries page
        self.driver.find_element_by_link_text("Countries").click()
        time.sleep(3)
        self.assertEqual(self.driver.current_url, link + "/countries")
        # Fill in search bar
        self.driver.find_element_by_id("localSearch").send_keys("Canada")
        time.sleep(2)
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/section[2]/div/div[1]/div/button').click()
        time.sleep(2)
        # Assert 1 element on page and that it is Canada
        self.assertEqual(len(self.driver.find_elements_by_xpath("/html/body/div/div/main/main/div/table/tbody/tr")), 1)
        self.assertEqual(self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/table/tbody/tr/td[1]/a').text, "Canada")

    def test_countries_filter_sort(self):
        # Go to Countries page
        self.driver.find_element_by_link_text("Countries").click()
        time.sleep(3)
        self.assertEqual(self.driver.current_url, link + "/countries")
        # Filter drop down
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/section[2]/div/div[2]/div[2]/button').click()
        time.sleep(3)
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/section[2]/div/div[2]/div[2]/div/button[1]').click()
        time.sleep(2)
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/section[2]/div/div[2]/button').click()
        time.sleep(3)
        # Confirm first table entry is correct
        self.assertEqual(self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/table/tbody/tr/td[1]/a').text, "Bermuda")
        # Sort drop down
        # Desc.
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/section[2]/div/div[3]/div[2]/button').click()
        time.sleep(3)
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/section[2]/div/div[3]/div[2]/div/button[2]').click()
        time.sleep(2)
        # Sort
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/section[2]/div/div[3]/div[3]/button').click()
        time.sleep(3)
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/section[2]/div/div[3]/div[3]/div/button[3]').click()
        time.sleep(2)
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/section[2]/div/div[3]/button').click()
        time.sleep(3)
        # Confirm first table entry is correct
        self.assertEqual(self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/table/tbody/tr/td[1]/a').text, "United States")

        # Clear All
        self.driver.find_element_by_xpath('/html/body/div/div/main/main/section[2]/div/button').click()
        time.sleep(3)
        # Confirm first table entry is correct
        self.assertEqual(self.driver.find_element_by_xpath('/html/body/div/div/main/main/div[2]/table/tbody/tr/td[1]/a').text, "Afghanistan")

        


        
    @classmethod
    def tearDownClass(cls):
        cls.driver.quit()


if __name__ == "__main__":
    main()