import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class WikipediaLink extends Component {
    constructor(props) {
        super(props);
        this.state = {
            link: null
        };
    };

    componentDidMount() {
        console.log("in wikipedia link");
        fetch('https://en.wikipedia.org/w/api.php?action=opensearch&origin=*&limit=1&namespace=0&format=json&search=' + this.props.search)
        .then((res) => res.json())
        .then((results) => {
            console.log(results);
            this.setState({link: results[3][0], linkTitle: results[0]});
        }).catch(console.log);
    }

    render() {
        return (
            <a href={this.state.link}>{this.state.linkTitle}</a>
        );
    }
};