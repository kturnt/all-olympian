import React, { Component } from "react";
import {
  Card,
  CardBody,
  CardText,
  CardTitle,
  CardImg,
  Button
} from "reactstrap";
import "../styles/CardDeckView.css";

class LocationsCompareCard extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const data = this.props.data;
    return (
      <Card className="my_compare_card">
        <CardImg
          className="card_image"
          src={data.logo_url}
          alt="Card image caption"
        />
        <CardBody>
          <CardTitle className="card_title">
            <b>City: </b>
            {data.city + " " + data.year}
          </CardTitle>
          <CardText className="card_text">
            <b>Hosting Country: </b>
            {data.hosting_country}
          </CardText>
          <CardText className="card_text">
            <b>Season: </b>
            {data.season.toUpperCase()}
          </CardText>
          <CardText className="card_text">
            <b>Top Athlete: </b>
            {data.top_athlete}
          </CardText>
          <CardText className="card_text">
            <b># of Athletes: </b>
            {data.num_athletes}
          </CardText>
          <CardText className="card_text">
            <b># of Countries: </b>
            {data.num_participating_countries}
          </CardText>
          <CardText className="card_text">
            <b># of Events: </b>
            {data.num_events}
          </CardText>
        </CardBody>
      </Card>
    );
  }
}

export default LocationsCompareCard;
