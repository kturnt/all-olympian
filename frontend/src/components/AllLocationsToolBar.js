import React, { Component } from "react";
import {
  ButtonToolbar,
  Button,
  ButtonDropdown,
  DropdownItem,
  DropdownMenu,
  InputGroupAddon,
  DropdownToggle,
  InputGroup,
  InputGroupText,
  Input
} from "reactstrap";
import "../styles/TableView.css";
import "../styles/Footer.css";

class AllLocationsToolBar extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ButtonToolbar className="search_bar">
        <InputGroup className="search_input">
          <Input
            id="localSearch"
            type="search"
            name="search"
            placeholder="Search"
            size="10"
            onChange={e => this.props.setSearchTerm(e.target.value)}
          />{" "}
          <InputGroupAddon addonType="append">
            <Button color="success" onClick={this.props.godSearch}>
              Go
            </Button>
          </InputGroupAddon>
        </InputGroup>

        <InputGroup className="filter_input">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>Filter By:</InputGroupText>
          </InputGroupAddon>
          <ButtonDropdown
            isOpen={this.props.seasonDropDownOpen}
            toggle={this.props.toggleSeason}
          >
            <DropdownToggle caret="true" color="secondary">
              {this.props.seasonDropDown}
            </DropdownToggle>
            <DropdownMenu className="search_bar_dropdown">
              <DropdownItem onClick={this.props.changeSeason}>Summer</DropdownItem>
              <DropdownItem onClick={this.props.changeSeason}>Winter</DropdownItem>
            </DropdownMenu>
          </ButtonDropdown>

          <ButtonDropdown
            isOpen={this.props.numEventsDropDownOpen}
            toggle={this.props.toggleNumEvents}
          >
            <DropdownToggle caret="true" color="secondary">
              {this.props.numEventsDropDown}
            </DropdownToggle>
            <DropdownMenu className="search_bar_dropdown">
              <InputGroup>
                <InputGroupText className="filter_dropdown_item">
                  <Input
                    placeholder="Min Value"
                    onChange={e => this.props.setMinNumEvents(+e.target.value)}
                  ></Input>
                </InputGroupText>
                <InputGroupText className="filter_dropdown_item">
                  <Input
                    placeholder="Max Value"
                    onChange={e => this.props.setMaxNumEvents(+e.target.value)}
                  ></Input>
                </InputGroupText>
                <InputGroupText className="filter_dropdown_item">
                  <Button color="success" onClick={this.props.changeNumEvents}>
                    Apply
                  </Button>
                </InputGroupText>
              </InputGroup>
            </DropdownMenu>
          </ButtonDropdown>

          <ButtonDropdown
            isOpen={this.props.numAthletesDropDownOpen}
            toggle={this.props.toggleNumAthletes}
          >
            <DropdownToggle caret="true" color="secondary">
              {this.props.numAthletesDropDown}
            </DropdownToggle>
            <DropdownMenu className="search_bar_dropdown">
              <InputGroup>
                <InputGroupText className="filter_dropdown_item">
                  <Input
                    placeholder="Min Value"
                    onChange={e => this.props.setMinNumAthletes(+e.target.value)}
                  ></Input>
                </InputGroupText>
                <InputGroupText className="filter_dropdown_item">
                  <Input
                    placeholder="Max Value"
                    onChange={e => this.props.setMaxNumAthletes(+e.target.value)}
                  ></Input>
                </InputGroupText>
                <InputGroupText className="filter_dropdown_item">
                  <Button color="success" onClick={this.props.changeNumAthletes}>
                    Apply
                  </Button>
                </InputGroupText>
              </InputGroup>
            </DropdownMenu>
          </ButtonDropdown>

          <ButtonDropdown
            isOpen={this.props.numCountriesDropDownOpen}
            toggle={this.props.toggleNumCountries}
          >
            <DropdownToggle caret="true" color="secondary">
              {this.props.numCountriesDropDown}
            </DropdownToggle>
            <DropdownMenu className="search_bar_dropdown">
              <InputGroup>
                <InputGroupText className="filter_dropdown_item">
                  <Input
                    placeholder="Min Value"
                    onChange={e => this.props.setMinNumCountries(+e.target.value)}
                  ></Input>
                </InputGroupText>
                <InputGroupText className="filter_dropdown_item">
                  <Input
                    placeholder="Max Value"
                    onChange={e => this.props.setMaxNumCountries(+e.target.value)}
                  ></Input>
                </InputGroupText>
                <InputGroupText className="filter_dropdown_item">
                  <Button color="success" onClick={this.props.changeNumCountries}>
                    Apply
                  </Button>
                </InputGroupText>
              </InputGroup>
            </DropdownMenu>
          </ButtonDropdown>

          <Button color="success" onClick={this.props.godSearch}>
            Filter
          </Button>
        </InputGroup>

        <InputGroup className="sort_input">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>Sort By:</InputGroupText>
          </InputGroupAddon>

          <ButtonDropdown
            isOpen={this.props.ascDropDownOpen}
            toggle={this.props.toggleAsc}
          >
            <DropdownToggle caret="true" color="secondary">
              {this.props.ascDropDown}
            </DropdownToggle>
            <DropdownMenu className="search_bar_dropdown">
              <DropdownItem onClick={this.props.changeAsc}>Asc.</DropdownItem>
              <DropdownItem onClick={this.props.changeAsc}>Desc.</DropdownItem>
            </DropdownMenu>
          </ButtonDropdown>

          <ButtonDropdown
            isOpen={this.props.sortDropDownOpen}
            toggle={this.props.toggleSort}
          >
            <DropdownToggle caret="true" color="secondary">
              {this.props.sortDropDown}
            </DropdownToggle>
            <DropdownMenu className="search_bar_dropdown">
              <DropdownItem onClick={this.props.changeSort}>
                Location City
              </DropdownItem>
              <DropdownItem onClick={this.props.changeSort}>Year</DropdownItem>
              <DropdownItem onClick={this.props.changeSort}>
                Hosting Country
              </DropdownItem>
              <DropdownItem onClick={this.props.changeSort}>Season</DropdownItem>
              <DropdownItem onClick={this.props.changeSort}>Athletes</DropdownItem>
              <DropdownItem onClick={this.props.changeSort}>Events</DropdownItem>
              <DropdownItem onClick={this.props.changeSort}>
                Participating Countries
              </DropdownItem>
            </DropdownMenu>
          </ButtonDropdown>

          <Button color="success" onClick={e => this.props.sortData()}>
            Sort
          </Button>
        </InputGroup>

        <Button
          color="danger"
          className="clear_all"
          onClick={e => this.props.resetFilters()}
        >
          Clear All
        </Button>
      </ButtonToolbar>
    );
  }
}
export default AllLocationsToolBar;
