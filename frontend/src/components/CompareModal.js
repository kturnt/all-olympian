import React, { Component, useState } from "react";
import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  CardDeck
} from "reactstrap";
import "../styles/CompareModal.css";

class CompareModal extends Component {
  constructor(props) {
    super(props);
    // our event handler for toggling our modal
    this.toggleModal = this.toggleModal.bind(this);
    this.state = {
      showModal: false
    };
  }

  toggleModal() {
    this.setState({ showModal: !this.state.showModal });
    console.log("compare-modal state: ", this.state.showModal);
  }

  render() {
    const CardType = this.props.compareCard;
    return (
      <section className="compare-modal">
        <Button
          disabled={this.props.compareGroup.length != 2}
          color="primary"
          onClick={this.toggleModal}
        >
          Compare <br></br> ({this.props.compareGroup.length} of 2 selected)
        </Button>
        <Modal
          size="xl"
          isOpen={this.state.showModal}
          toggle={this.toggleModal}
          className="my-modal"
        >
          <ModalHeader toggle={this.toggleModal} className="modal-header">
            Compare
          </ModalHeader>
          <ModalBody>
            <CardDeck>
              {
                (console.log("inside modal body", this.props.compareGroup),
                this.props.compareGroup.map((compare, i) => (
                  <CardType data={compare} />
                )))
              }
            </CardDeck>
          </ModalBody>
          <ModalFooter>
            <Button color="secondary" onClick={this.toggleModal}>
              Hide
            </Button>
          </ModalFooter>
        </Modal>
      </section>
    );
  }
}

export default CompareModal;
