import React, { Component } from "react";
import {
  ButtonToolbar,
  Button,
  ButtonDropdown,
  DropdownItem,
  DropdownMenu,
  InputGroupAddon,
  DropdownToggle,
  InputGroup,
  InputGroupText,
  Input
} from "reactstrap";
import "../styles/TableView.css";
import "../styles/Footer.css";

class AllEventsToolBar extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <ButtonToolbar className="search_bar">
        <InputGroup className="search_input">
          <Input
            id="localSearch"
            type="search"
            name="search"
            placeholder="Search"
            size="10"
            onChange={e => this.props.setSearchTerm(e.target.value)}
          />{" "}
          <InputGroupAddon addonType="append">
            <Button color="success" onClick={this.props.godSearch}>
              Go
            </Button>
          </InputGroupAddon>
        </InputGroup>
        <InputGroup className="filter_input">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>Filter By:</InputGroupText>
          </InputGroupAddon>
          <ButtonDropdown
            isOpen={this.props.genderDropDownOpen}
            toggle={this.props.toggleGender}
          >
            <DropdownToggle caret="true" color="secondary">
              {this.props.genderDropDown}
            </DropdownToggle>
            <DropdownMenu className="search_bar_dropdown">
              <DropdownItem onClick={this.props.changeGender}>
                Male
              </DropdownItem>
              <DropdownItem onClick={this.props.changeGender}>
                Female
              </DropdownItem>
            </DropdownMenu>
          </ButtonDropdown>
          <ButtonDropdown
            isOpen={this.props.seasonDropDownOpen}
            toggle={this.props.toggleSeason}
          >
            <DropdownToggle caret="true" color="secondary">
              {this.props.seasonDropDown}
            </DropdownToggle>
            <DropdownMenu className="search_bar_dropdown">
              <DropdownItem onClick={this.props.changeSeason}>
                Summer
              </DropdownItem>
              <DropdownItem onClick={this.props.changeSeason}>
                Winter
              </DropdownItem>
            </DropdownMenu>
          </ButtonDropdown>

          <Button color="success" onClick={this.props.godSearch}>
            Filter
          </Button>
        </InputGroup>

        <InputGroup className="sort_input">
          <InputGroupAddon addonType="prepend">
            <InputGroupText>Sort By:</InputGroupText>
          </InputGroupAddon>
          <ButtonDropdown
            isOpen={this.props.sortDropDownOpen}
            toggle={this.props.toggleSort}
          >
            <DropdownToggle caret="true" color="secondary">
              {this.props.sortDropDown}
            </DropdownToggle>
            <DropdownMenu className="search_bar_dropdown">
              <DropdownItem onClick={this.props.changeSort}>
                Event Name
              </DropdownItem>
              <DropdownItem onClick={this.props.changeSort}>
                Sport Name
              </DropdownItem>
              <DropdownItem onClick={this.props.changeSort}>
                Gender
              </DropdownItem>
              <DropdownItem onClick={this.props.changeSort}>
                Season
              </DropdownItem>
              <DropdownItem onClick={this.props.changeSort}>
                Debut Year
              </DropdownItem>
            </DropdownMenu>
          </ButtonDropdown>

          <ButtonDropdown
            isOpen={this.props.ascDropDownOpen}
            toggle={this.props.toggleAsc}
          >
            <DropdownToggle caret="true" color="secondary">
              {this.props.ascDropDown}
            </DropdownToggle>
            <DropdownMenu className="search_bar_dropdown">
              <DropdownItem onClick={this.props.changeAsc}>Asc.</DropdownItem>
              <DropdownItem onClick={this.props.changeAsc}>Desc.</DropdownItem>
            </DropdownMenu>
          </ButtonDropdown>

          <Button color="success" onClick={e => this.props.sortData()}>
            Sort
          </Button>
        </InputGroup>

        <Button
          color="danger"
          className="clear_all"
          onClick={this.props.resetFilters}
        >
          Clear All
        </Button>
      </ButtonToolbar>
    );
  }
}
export default AllEventsToolBar;
