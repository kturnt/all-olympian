import React, { Component } from "react";
import {
  Card,
  CardBody,
  CardText,
  CardTitle,
  CardImg,
  Button
} from "reactstrap";
import "../styles/CardDeckView.css";

class CountriesCompareCard extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const data = this.props.data;
    return (
      <Card className="my_compare_card">
        <CardImg
          className="card_image"
          src={data.capital_image_url}
          alt="Card image caption"
        />
        <CardBody>
          <CardTitle>
              <b>Country: </b>
              {data.display_name}
          </CardTitle>
          <CardText className="card_title">
            <b>Capital: </b>
            {data.capital}
          </CardText>
          <CardText className="card_text">
            <b>GDP: </b>
            {data.gdp}
          </CardText>
          <CardText className="card_text">
            <b>Population: </b>
            {data.population}
          </CardText>
          <CardText className="card_text">
            <b>Region: </b> {data.region}
          </CardText>
          <CardText className="card_text">
            <b>Medal Count: </b>
            {data.medal_count}
          </CardText>
        </CardBody>
      </Card>
    );
  }
}

export default CountriesCompareCard;
