import React, {Component} from 'react';
import {
  Navbar, NavbarBrand, Nav, NavItem, NavLink, DropdownToggle, DropdownMenu, NavbarText, UncontrolledDropdown, DropdownItem, NavbarToggler, Collapse, Form, Input, InputGroup, InputGroupAddon, Button
} from 'reactstrap';
import { useState } from 'react';
import { Link } from 'react-router-dom'
import { useHistory } from "react-router-dom";
import queryString from 'query-string';
import logo from "../images/logo_transparent.png"
import "../styles/Home.css"

class Header extends Component {

  constructor(props) {
    super(props);
    this.state = {
      searchText: null
    };
  }

  render() {
    const { searchText } = this.state;
      return (
        <div>
          <Navbar color="light" light expand="md" className="navbar">
            <NavbarBrand href="/"><img src={logo} width="140px" height="45px" image="pixelated"></img></NavbarBrand>

              <Nav className="mr-auto" navbar>
                <NavItem>
                  <NavLink href="/events">Events</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/countries">Countries</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/locations">Locations</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/visualizations">Visualizations</NavLink>
                </NavItem>
                <NavItem>
                  <NavLink href="/about">About</NavLink>
                </NavItem>
                </Nav>
                  <Form inline="true" action="/search">
                      <InputGroup>
                        <Input type="search" name="query" value={searchText} onChange={this.textChanged} placeholder="Search"/>{' '}
                        <InputGroupAddon addonType="append"><Button color="success">Go</Button></InputGroupAddon>
                      </InputGroup>
                  </Form>
          </Navbar>
        </div>);
  }

  componentDidMount() {
      const values = queryString.parse(window.location.search)
      console.log('search text is')
      console.log(values.query)
      this.setState({searchText: values.query})
  }

  textChanged = () => {
    this.setState({searchText: null})
  }
}

export default Header;
 