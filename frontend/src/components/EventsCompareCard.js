import React, { Component } from "react";
import {
  Card,
  CardBody,
  CardText,
  CardTitle,
  CardImg,
  Button
} from "reactstrap";
import "../styles/CardDeckView.css";

class EventsCompareCard extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const data = this.props.data;
    return (
      <Card className="my_compare_card">
        <CardImg
          className="card_image"
          src={data.image_url}
          alt="Card image caption"
        />
        <CardBody>
          <CardTitle className="card_title">
            <b>Event: </b>
            {data.event}
          </CardTitle>
          <CardText className="card_text">
            <b>Sport: </b>
            {data.overarching_sport}
          </CardText>
          <CardText className="card_text">
            <b>Gender: </b>
            {data.gender.charAt(0).toUpperCase() + data.gender.slice(1)}
          </CardText>
          <CardText className="card_text">
            <b>Season: </b>
            {data.season.charAt(0).toUpperCase() + data.season.slice(1)}
          </CardText>
          {data.debut_year != null ? (
            <CardText className="card_text">
              <b>Debut Year: </b>
              {data.debut_year.toString()}
            </CardText>
          ) : (
            ""
          )}
          <CardText className="card_text">
            <b>Top Athlete: </b>
            {data.top_athlete}
          </CardText>
          <CardText className="card_text">
            <b>Top Athlete Country: </b>
            {data.top_athlete_country}
          </CardText>
          <CardText className="card_text">
            <b>Latest Champion: </b>
            {data.latest_champion}
          </CardText>
          <CardText className="card_text">
            <b>Latest Champion Country: </b>
            {data.latest_champion_country}
          </CardText>
        </CardBody>
      </Card>
    );
  }
}

export default EventsCompareCard;
