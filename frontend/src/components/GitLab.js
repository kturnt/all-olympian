import React, { Component } from 'react';
import Members from '../Members'
import {
	CardFooter, ListGroup, ListGroupItem, Card, CardImg, CardTitle, CardText, CardHeader, CardDeck, CardSubtitle, CardBody
} from 'reactstrap'
import '../styles/About.css';


export default class GitLab extends Component {
	constructor(props) {
		super(props);
		this.state = {
			commits: [],
			issues: [],
		};
	}

	async componentDidMount() {
		 // fetch commits
		 await fetch('https://gitlab.com/api/v4/projects/17112868/repository/commits?per_page=100&page=1')
		 .then((res) => res.json())
		 .then((commits) => {
		 	this.setState({ commits })
		 }).catch(console.log)

		 await fetch('https://gitlab.com/api/v4/projects/17112868/repository/commits?per_page=100&page=2')
		 .then((res) => res.json())
		 .then((moreCommits) => {
			let temp = this.state.commits.concat(moreCommits)
			this.setState({commits: temp})
		 })

		 await fetch('https://gitlab.com/api/v4/projects/17112868/repository/commits?per_page=100&page=3')
		 .then((res) => res.json())
		 .then((moreCommits) => {
			let temp = this.state.commits.concat(moreCommits)
			this.setState({commits: temp})
		 })

		 await fetch('https://gitlab.com/api/v4/projects/17112868/issues?per_page=100&page=1')
		 .then((res) => res.json())
		 .then((issues) => {
		 	this.setState({ issues })
		 }).catch(console.log)
	}

	mapCommitCount() {
		for (const member of Members) {
			var commitCount = 0;
			var issueCount = 0;

			for (const commit of this.state.commits) {
				if (member.name === commit['author_name']) {
					commitCount++;
				}
				console.log(commit['author_name'])
				if(member.name === "Max Patrick" && (!['Laith Alsukhni', 'Ryan Resma', 'Kevin Turner', 'Campbell Sinclair'].includes(commit["author_name"]))) {
					commitCount++;
				}
			}

			for (const issue of this.state.issues) {
				const assignees = issue.assignees;
				if (assignees.length > 0) {
					for (const assignee of assignees) {
						if (assignee['username'] == member.username) {
							issueCount++;
						}
					}
				}
			}

			member['commitCount'] = commitCount;
			member['issueCount'] = issueCount;
			member['testCount'] = 5
		}
	}


	render() {
		this.mapCommitCount()
		return (
			<section>	
				<div className="team-card-deck">
					<CardDeck>
						{Members.map((member) => {
							return  (		
								<Card className="team-card">
									<CardImg className="gitlab-image" src={member.avatarLink}/>
									<CardHeader>
										<CardTitle className="member-name">{member.name}</CardTitle>
										<CardText>{member.role}</CardText>
									</CardHeader>
									<CardHeader>
										<CardText className="gitlab-bio">{member.bio}</CardText>
									</CardHeader>
									<CardFooter className="team-card-footer">
										<CardSubtitle className="gitlab-stats-header">Gitlab Stats</CardSubtitle>
										<ListGroup>
											<ListGroupItem className="gitlab-stat">Commits: {member.commitCount}</ListGroupItem>
											<ListGroupItem className="gitlab-stat">Issues: {member.issueCount}</ListGroupItem>
											<ListGroupItem className="gitlab-stat">Unit Tests: {member.testCount}</ListGroupItem>
										</ListGroup>
									</CardFooter>
							</Card>)
						})}
					</CardDeck>
				</div>
				<div className="project-stats">
					<h1>Project Gitlab Stats</h1>
					<ListGroup className="project-stats-list">
						<ListGroupItem className="project-stat">{this.state.commits.length} Total Commits</ListGroupItem>
						<ListGroupItem className="project-stat">{this.state.issues.length} Total Issues</ListGroupItem>
						<ListGroupItem className="project-stat">25 Unit Total Tests</ListGroupItem>
					</ListGroup>
				</div>
			</section>
		)
	}
};
