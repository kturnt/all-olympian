import React, { Component, useState } from "react";
import { Link } from "react-router-dom";
import {
  UncontrolledCollapse,
  ButtonToolbar,
  Button,
  ButtonGroup,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink,
  Jumbotron,
  ButtonDropdown,
  DropdownItem,
  DropdownMenu,
  InputGroupAddon,
  DropdownToggle,
  InputGroup,
  InputGroupText,
  Input
} from "reactstrap";
import "../styles/TableView.css";
import "../styles/Footer.css";

const numPerPage = 10;

class AllPagination extends Component {
  constructor(props) {
    super(props);
  }
  render() {
    return (
      <section class="footer">
        <div class="pagination">
          <ul class="pagination justify-content-center">
            <Pagination aria-label="Page navigation example">
              <PaginationItem disabled={this.props.page <= 1}>
                <PaginationLink
                  onClick={e => this.props.getPage(1)}
                  first
                  href="#"
                />
              </PaginationItem>
              <PaginationItem hidden={this.props.page <= 2}>
                <PaginationLink
                  onClick={e => this.props.getPage(this.props.page - 2)}
                  previous
                  href="#"
                >
                  {this.props.page - 2}
                </PaginationLink>
              </PaginationItem>
              <PaginationItem hidden={this.props.page <= 1}>
                <PaginationLink
                  onClick={e => this.props.getPage(this.props.page - 1)}
                  previous
                  href="#"
                >
                  {this.props.page - 1}
                </PaginationLink>
              </PaginationItem>
              <PaginationItem disabled>
                <PaginationLink href="#">{this.props.page}</PaginationLink>
              </PaginationItem>
              <PaginationItem
                hidden={
                  this.props.page >=
                  Math.ceil(this.props.data.length / numPerPage)
                }
              >
                <PaginationLink
                  onClick={e => this.props.getPage(this.props.page + 1)}
                  next
                  href="#"
                >
                  {this.props.page + 1}
                </PaginationLink>
              </PaginationItem>
              <PaginationItem
                hidden={
                  this.props.page >=
                  Math.ceil(this.props.data.length / numPerPage) - 1
                }
              >
                <PaginationLink
                  onClick={e => this.props.getPage(this.props.page + 2)}
                  next
                  href="#"
                >
                  {this.props.page + 2}
                </PaginationLink>
              </PaginationItem>
              <PaginationItem
                disabled={
                  this.props.page >=
                  Math.ceil(this.props.data.length / numPerPage)
                }
              >
                <PaginationLink
                  onClick={e =>
                    this.props.getPage(
                      Math.ceil(this.props.data.length / numPerPage)
                    )
                  }
                  last
                  href="#"
                />
              </PaginationItem>
            </Pagination>
          </ul>
        </div>
        Page {this.props.page} of{" "}
        {Math.ceil(this.props.data.length / numPerPage)}
        <br></br>
        Displaying {(this.props.page - 1) * numPerPage + 1} -{" "}
        {this.props.page * numPerPage > this.props.data.length
          ? this.props.data.length
          : this.props.page * numPerPage}{" "}
        of {this.props.data.length}
      </section>
    );
  }
}
export default AllPagination;
