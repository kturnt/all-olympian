import React, { Component, useState } from "react";
import {
  ButtonToolbar,
  Button,
  ButtonDropdown,
  DropdownItem,
  DropdownMenu,
  InputGroupAddon,
  DropdownToggle,
  InputGroup,
  InputGroupText,
  Input
} from "reactstrap";
import "../styles/TableView.css";
import "../styles/Footer.css";

class AllCountriesToolBar extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <section>
        <ButtonToolbar className="countries_searchbar">
          <InputGroup className="search_input">
            <Input
              id="localSearch"
              type="search"
              name="search"
              placeholder="Search"
              size="10"
              onChange={e => this.props.setSearchTerm(e.target.value)}
            />{" "}
            <InputGroupAddon addonType="append">
              <Button color="success" onClick={this.props.godSearch}>
                Go
              </Button>
            </InputGroupAddon>
          </InputGroup>
          <InputGroup className="filter_input">
            <InputGroupAddon addonType="prepend">
              <InputGroupText>Filter By:</InputGroupText>
            </InputGroupAddon>
            <ButtonDropdown
              isOpen={this.props.regionDropDownOpen}
              toggle={this.props.toggleRegion}
            >
              <DropdownToggle caret="true" color="secondary">
                {this.props.regionDropDown}
              </DropdownToggle>
              <DropdownMenu className="search_bar_dropdown">
                <DropdownItem onClick={this.props.changeRegion}>
                  North America
                </DropdownItem>
                <DropdownItem onClick={this.props.changeRegion}>
                  Latin America & Caribbean
                </DropdownItem>
                <DropdownItem onClick={this.props.changeRegion}>
                  South Asia
                </DropdownItem>
                <DropdownItem onClick={this.props.changeRegion}>
                  East Asia & Pacific
                </DropdownItem>
                <DropdownItem onClick={this.props.changeRegion}>
                  Middle East & North Africa
                </DropdownItem>
                <DropdownItem onClick={this.props.changeRegion}>
                  Sub-Saharan Africa
                </DropdownItem>
                <DropdownItem onClick={this.props.changeRegion}>
                  Europe & Central Asia
                </DropdownItem>
              </DropdownMenu>
            </ButtonDropdown>

            <ButtonDropdown
              isOpen={this.props.populationDropDownOpen}
              toggle={this.props.togglePopulation}
            >
              <DropdownToggle caret="true" color="secondary">
                {this.props.populationDropDown}
              </DropdownToggle>
              <DropdownMenu className="search_bar_dropdown">
                <InputGroup>
                  <InputGroupText className="filter_dropdown_item">
                    <Input
                      placeholder="Min Value"
                      type="number"
                      onChange={e => this.props.setMinPop(+e.target.value)}
                    ></Input>
                  </InputGroupText>
                  <InputGroupText className="filter_dropdown_item">
                    <Input
                      placeholder="Max Value"
                      type="number"
                      onChange={e => this.props.setMaxPop(+e.target.value)}
                    ></Input>
                  </InputGroupText>
                  <InputGroupText className="filter_dropdown_item">
                    <Button
                      color="success"
                      onClick={this.props.changePopulation}
                    >
                      Apply
                    </Button>
                  </InputGroupText>
                </InputGroup>
              </DropdownMenu>
            </ButtonDropdown>

            <ButtonDropdown
              isOpen={this.props.GDPDropDownOpen}
              toggle={this.props.toggleGDP}
            >
              <DropdownToggle caret="true" color="secondary">
                {this.props.GDPDropDown}
              </DropdownToggle>
              <DropdownMenu className="search_bar_dropdown">
                <InputGroup>
                  <InputGroupText className="filter_dropdown_item">
                    <Input
                      placeholder="Min Value"
                      type="number"
                      onChange={e => this.props.setMinGDP(+e.target.value)}
                    ></Input>
                  </InputGroupText>
                  <InputGroupText className="filter_dropdown_item">
                    <Input
                      placeholder="Max Value"
                      type="number"
                      onChange={e => this.props.setMaxGDP(+e.target.value)}
                    ></Input>
                  </InputGroupText>
                  <InputGroupText className="filter_dropdown_item">
                    <Button color="success" onClick={this.props.changeGDP}>
                      Apply
                    </Button>
                  </InputGroupText>
                </InputGroup>
              </DropdownMenu>
            </ButtonDropdown>

            <ButtonDropdown
              isOpen={this.props.medalDropDownOpen}
              toggle={this.props.toggleMedal}
            >
              <DropdownToggle caret="true" color="secondary">
                {this.props.medalDropDown}
              </DropdownToggle>
              <DropdownMenu className="search_bar_dropdown">
                <InputGroup>
                  <InputGroupText className="filter_dropdown_item">
                    <Input
                      placeholder="Min Value"
                      type="number"
                      onChange={e => this.props.setMinMedal(+e.target.value)}
                    ></Input>
                  </InputGroupText>
                  <InputGroupText className="filter_dropdown_item">
                    <Input
                      placeholder="Max Value"
                      type="number"
                      onChange={e => this.props.setMaxMedal(+e.target.value)}
                    ></Input>
                  </InputGroupText>
                  <InputGroupText className="filter_dropdown_item">
                    <Button color="success" onClick={this.props.changeMedal}>
                      Apply
                    </Button>
                  </InputGroupText>
                </InputGroup>
              </DropdownMenu>
            </ButtonDropdown>

            <Button color="success" onClick={this.props.godSearch}>
              Filter
            </Button>
          </InputGroup>

          <InputGroup className="sort_input">
            <InputGroupAddon addonType="prepend">
              <InputGroupText>Sort By:</InputGroupText>
            </InputGroupAddon>
            <ButtonDropdown
              isOpen={this.props.sortDropDownOpen}
              toggle={this.props.toggleSort}
            >
              <DropdownToggle caret="true" color="secondary">
                {this.props.sortDropDown}
              </DropdownToggle>
              <DropdownMenu className="search_bar_dropdown">
                <DropdownItem onClick={this.props.changeSort}>
                  Region
                </DropdownItem>
                <DropdownItem onClick={this.props.changeSort}>
                  Country Name
                </DropdownItem>
                <DropdownItem onClick={this.props.changeSort}>
                  IOC Code
                </DropdownItem>
                <DropdownItem onClick={this.props.changeSort}>
                  Medal Count
                </DropdownItem>
                <DropdownItem onClick={this.props.changeSort}>
                  Population
                </DropdownItem>
                <DropdownItem onClick={this.props.changeSort}>GDP</DropdownItem>
              </DropdownMenu>
            </ButtonDropdown>

            <ButtonDropdown
              isOpen={this.props.ascDropDownOpen}
              toggle={this.props.toggleAsc}
            >
              <DropdownToggle caret="true" color="secondary">
                {this.props.ascDropDown}
              </DropdownToggle>
              <DropdownMenu className="search_bar_dropdown">
                <DropdownItem onClick={this.props.changeAsc}>Asc.</DropdownItem>
                <DropdownItem onClick={this.props.changeAsc}>
                  Desc.
                </DropdownItem>
              </DropdownMenu>
            </ButtonDropdown>
            <Button color="success" onClick={this.props.sortData}>
              Sort
            </Button>
          </InputGroup>

          <Button
            color="danger"
            className="clear_all"
            onClick={e => this.props.resetFilters()}
          >
            Clear All
          </Button>
        </ButtonToolbar>
      </section>
    );
  }
}
export default AllCountriesToolBar;
