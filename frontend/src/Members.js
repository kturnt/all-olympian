export const Members = [
  {
    name: "Kevin Turner",
    username: "kturnt",
    role:
      "Full Stack: AWS Management, PostgreSQL Management, Scrum Master/Project Leader, Backend Setup and Testing, Gitlab CI, Docker Setup",
    bio:
      "Third year CS student who's Big Chungus Architect and strictly does cardio.",
    avatarLink:
      "https://gitlab.com/uploads/-/system/user/avatar/4569571/avatar.png?width=400",
    commitCount: 0,
    issueCount: 0,
    testCount: 0
  },
  {
    name: "Ryan Resma",
    username: "rresma99",
    role:
      "Front-End: Data Acquisition, React Development, Web-Scraping, CSS Styling, Mocha Unit Testing",
    bio:
      "Third year CS student who enjoys studying to become a CSS god, lifting, and driving the boat in LiveShare.",
    avatarLink:
      "https://gitlab.com/uploads/-/system/user/avatar/4532196/avatar.png?width=400",
    commitCount: -1,
    issueCount: -10,
    testCount: -1000
  },
  {
    name: "Laith Alsukhni",
    username: "laith_alsukhni",
    role:
      "Front-End: React Development, Data Acquisition, Pagination, CSS Styling, GUI Testing",
    bio:
      "Beautiful Senior CS student who's really out here just trying to graduate. Don't even look at me right now.",
    avatarLink:
      "https://gitlab.com/uploads/-/system/user/avatar/3500822/avatar.png?width=400",
    commitCount: -1,
    issueCount: -10,
    testCount: -1000
  },
  {
    name: "Max Patrick",
    username: "maxpat",
    role:
      "Front-End: Data Acquisition, React Development, Web-Scraping, Mocha Unit Testing, Pair Programming God",
    bio:
      "Third year CS student who loves lifting, playing games, and clowning. Academic officer for HACS.",
    avatarLink:
      "https://gitlab.com/uploads/-/system/user/avatar/4589874/avatar.png?width=400",
    commitCount: -1,
    issueCount: -10,
    testCount: -1000
  },
  {
    name: "Campbell Sinclair",
    username: "csinclair",
    role:
      "Back-End: Data Acquisition, Web Scraping, Postman API, RESTful API Discovery and Utilization, API Testing, Model Connections",
    bio:
      "Third year CS student who's interested in cybersecurity and plays piano.",
    avatarLink:
      "https://gitlab.com/uploads/-/system/user/avatar/5274376/avatar.png?width=400",
    commitCount: -1,
    issueCount: -10,
    testCount: -1000
  }
];

export default Members;
