import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Location from '../pages/Location'
import AllLocations from '../pages/AllLocations'

function LocationsRouter() {
	return (
		<Switch>
			<Route exact path='/locations' component={AllLocations}/>
			<Route path='/locations/:locationId' component={Location}/>
		</Switch>
	);
}

export default LocationsRouter;
