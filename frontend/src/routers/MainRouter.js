import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from '../pages/Home'
import CountriesRouter from './CountriesRouter'
import LocationsRouter from './LocationsRouter'
import EventsRouter from './EventsRouter'
import About from '../pages/About'
import Search from '../pages/Search'
import Visualizations from '../pages/Visualizations'


const MainRouter = () => (
  <main>
    <Switch>
      <Route exact path='/' component={Home}/>
      <Route path='/countries' component={CountriesRouter}/>
      <Route path='/events' component={EventsRouter}/>
      <Route path='/locations' component={LocationsRouter}/>
      <Route path='/about' component={About}/>
      <Route path='/search' component={Search}/>
      <Route path='/visualizations' component={Visualizations}/>
    </Switch>
  </main>
)

export default MainRouter;
