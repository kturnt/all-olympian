import React from 'react'
import { Switch, Route } from 'react-router-dom'
import Country from '../pages/Country'
import AllCountries from '../pages/AllCountries'
import About from '../pages/About'

function CountriesRouter() {
	return (
		<Switch>
			<Route exact path='/countries' component={AllCountries}/>
			<Route path='/countries/:countryId' component={Country}/>
		</Switch>

	);
}

export default CountriesRouter;
