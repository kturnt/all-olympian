import React from 'react'
import { Switch, Route } from 'react-router-dom'
import AllEvents from '../pages/AllEvents'
import Event from '../pages/Event'

function EventsRouter() {
	return (
		<Switch>
			<Route exact path='/events' component={AllEvents}/>
			<Route path='/events/:eventId' component={Event}/>
		</Switch>
	);
}

export default EventsRouter;
