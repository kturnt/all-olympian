import React, { Component } from "react";
import YouTube from 'react-youtube'
import { Link } from "react-router-dom";
import {
  UncontrolledCollapse,
  Button,
  Table,
  Container,
  Spinner,
  ListGroup,
  ListGroupItem
} from "reactstrap";
import "../styles/SingleCountry.css";
import { YoutubeURL, YoutubeOpts } from '../Youtube'
import WikipediaLink from '../components/WikipediaLink';

class Country extends Component {
  constructor(props) {
    super(props);
    this.state = {
      athletes: null,
      capital: null,
      capital_image_url: null,
      country_flag_url: null,
      olympics_image_url: null,
      display_name: null,
      gdp: null,
      ioc_code: null,
      languages: null,
      locations_participated: null,
      locations_info: null,
      events_participated: null,
      ep_info: null,
      events_medaled: null,
      em_info: null,
      events_with_top_athlete: null,
      eta_info: null,
      events_with_latest_champ: null,
      elc_info: null,
      medal_count: null,
      medals: null,
      name: null,
      population: null,
      region: null,
      isLoaded: false
    };
  }

  async componentDidMount() {
    //ioc should be this.props.params.match
    const ioc = this.props.match.params.countryId;
    const url = "https://api.all-olympian.com//api/countries/" + ioc;
    await fetch(url)
      .then(res => res.json())
      .then(data => {
        this.setState({
          athletes: data.athletes,
          capital: data.capital,
          capital_image_url: data.capital_image_url,
          country_flag_url: data.country_flag_url,
          olympics_image_url: data.olympics_image_url,
          display_name: data.display_name,
          gdp: data.gdp,
          ioc_code: data.ioc_code,
          languages: data.languages,
          locations_participated: data.locations_participated,
          events_participated: data.all_events,
          events_medaled: data.medaled_events,
          events_with_top_athlete: data.top_athl_in,
          events_with_latest_champ: data.latest_champ_in,
          medal_count: data.medal_count,
          medals: data.medals,
          name: data.name,
          population: data.population,
          region: data.region,
          isLoaded: true
        });
        console.log("fetching from API 1 DONE");
      })
      .then(() => {
        //LOCATIONS PARTICIPATED
        const baseurl =
          "https://ja1sq0kcre.execute-api.us-west-2.amazonaws.com/api/olympics?ids=";
        var toappend = "";
        console.log("trying to get locations now");
        for (var i = 0; i < this.state.locations_participated.length - 1; i++) {
          toappend += this.state.locations_participated[i] + ",";
        }
        toappend += this.state.locations_participated[
          this.state.locations_participated.length - 1
        ];
        const tofetch = baseurl + toappend;
        fetch(tofetch)
          .then(locations => locations.json())
          .then(locations_data => {
            var tempList = [];
            for (var j = 0; j < locations_data.length; j++) {
              var toAdd = [];
              var tempLoc =
                locations_data[j].city + " " + locations_data[j].year;
              var tempLink = locations_data[j].id;
              toAdd.push(tempLoc);
              toAdd.push(tempLink);
              tempList.push(toAdd);
            }
            this.setState({
              locations_info: tempList
            });
            console.log("fetching from API 2 DONE");
          });
      })
      .then(() => {
        //EVENTS PARTICIPATED
        const baseurl =
          "https://ja1sq0kcre.execute-api.us-west-2.amazonaws.com/api/events?ids=";
        if (this.state.events_participated.length != 0) {
          console.log("ALL EVENTS " + this.state.events_participated.length);
          var toappend = "";
          console.log("trying to get events participated now");
          var theMax = Math.min(100, this.state.events_participated.length);
          for (var i = 0; i < theMax - 1; i++) {
            toappend += this.state.events_participated[i] + ",";
          }
          toappend += this.state.events_participated[theMax - 1];
          const toFetch1 = baseurl + toappend;
          console.log("All events " + toFetch1);
          fetch(toFetch1)
            .then(ep => ep.json())
            .then(ep_data => {
              var tempList = [];
              for (var j = 0; j < ep_data.length; j++) {
                var toAdd = [];
                var tempEvent =
                  ep_data[j].overarching_sport +
                  " " +
                  ep_data[j].event +
                  " (" +
                  ep_data[j].gender +
                  ")";
                var tempLink = ep_data[j].id;
                toAdd.push(tempEvent);
                toAdd.push(tempLink);
                tempList.push(toAdd);
              }
              this.setState({
                ep_info: tempList
              });
              console.log("fetching from API 3 DONE");
            });
        }
        //EVENTS MEDALED
        if (this.state.events_medaled.length != 0) {
          var toappend = "";
          console.log("trying to get events medaled now");
          var theMax = Math.min(100, this.state.events_medaled.length);
          for (var i = 0; i < theMax - 1; i++) {
            toappend += this.state.events_medaled[i] + ",";
          }
          toappend += this.state.events_medaled[theMax - 1];
          const toFetch2 = baseurl + toappend;
          fetch(toFetch2)
            .then(em => em.json())
            .then(em_data => {
              var tempList = [];
              for (var j = 0; j < em_data.length; j++) {
                var toAdd = [];
                var tempEvent =
                  em_data[j].overarching_sport +
                  " " +
                  em_data[j].event +
                  " (" +
                  em_data[j].gender +
                  ")";
                var tempLink = em_data[j].id;
                toAdd.push(tempEvent);
                toAdd.push(tempLink);
                tempList.push(toAdd);
              }
              this.setState({
                em_info: tempList
              });
              console.log("fetching from API 4 DONE");
            });
        }
        //EVENTS WITH TOP ATHL
        if (this.state.events_with_top_athlete.length != 0) {
          var toappend = "";
          console.log("trying to get events with top athl now");
          var theMax = Math.min(100, this.state.events_with_top_athlete.length);
          for (var i = 0; i < theMax - 1; i++) {
            toappend += this.state.events_with_top_athlete[i] + ",";
          }
          toappend += this.state.events_with_top_athlete[theMax - 1];
          const toFetch3 = baseurl + toappend;
          fetch(toFetch3)
            .then(eta => eta.json())
            .then(eta_data => {
              var tempList = [];
              for (var j = 0; j < eta_data.length; j++) {
                var toAdd = [];
                var tempEvent =
                  eta_data[j].overarching_sport +
                  " " +
                  eta_data[j].event +
                  " (" +
                  eta_data[j].gender +
                  ")";
                var tempLink = eta_data[j].id;
                toAdd.push(tempEvent);
                toAdd.push(tempLink);
                tempList.push(toAdd);
              }
              this.setState({
                eta_info: tempList
              });
              console.log("fetching from API 5 DONE");
            });
        }
        //EVENTS WITH LATEST CHAMPION
        if (this.state.events_with_latest_champ.length != 0) {
          var toappend = "";
          console.log("trying to get events with latest athl now");
          var theMax = Math.min(
            100,
            this.state.events_with_latest_champ.length
          );
          for (var i = 0; i < theMax - 1; i++) {
            toappend += this.state.events_with_latest_champ[i] + ",";
          }
          toappend += this.state.events_with_latest_champ[theMax - 1];
          const toFetch4 = baseurl + toappend;
          fetch(toFetch4)
            .then(elc => elc.json())
            .then(elc_data => {
              var tempList = [];
              for (var j = 0; j < elc_data.length; j++) {
                var toAdd = [];
                var tempEvent =
                  elc_data[j].overarching_sport +
                  " " +
                  elc_data[j].event +
                  " (" +
                  elc_data[j].gender +
                  ")";
                var tempLink = elc_data[j].id;
                toAdd.push(tempEvent);
                toAdd.push(tempLink);
                tempList.push(toAdd);
              }
              this.setState({
                elc_info: tempList
              });
              console.log("fetching from API 6 DONE");
            });
        }
      })
      .catch(console.log);

    const query = "info about " + this.state.display_name + " country"
    const youtubeUrl = YoutubeURL + "&q=" + query
    await fetch(youtubeUrl)
    .then(res => res.json())
    .then(
	    (data) => {
		    this.setState({
			    video_id: data.items[0].id.videoId
		    });
		    console.log("SEARCHED FOR VIDEO ID " + this.state.video_id)
	    }
    )
  }

  render() {
    const {
      athletes,
      capital,
      capital_image_url,
      country_flag_url,
      olympics_image_url,
      display_name,
      gdp,
      ioc_code,
      languages,
      locations_participated,
      locations_info,
      events_participated,
      ep_info,
      events_medaled,
      em_info,
      events_with_top_athlete,
      eta_info,
      events_with_latest_champ,
      elc_info,
      medal_count,
      medals,
      name,
      population,
      region,
      isLoaded,
      video_id
    } = this.state;

    if (!isLoaded) {
      return (
        <div>
          LOADING...
          <Spinner type="grow" color="primary" />
        </div>
      );
    } else {
      return (
        <main>
          <section class="country_header">
            <div class="holder">
              <h1 class="country_name">
                {<img class="flagImg" src={this.state.country_flag_url}></img>}{" "}
                {display_name}{" "}
                {<img class="flagImg" src={this.state.country_flag_url}></img>}
              </h1>
            </div>
          </section>
          <br></br>
          <section className="capitalImage">
            <div class="holder">
              <img class="theImg" src={this.state.capital_image_url}></img>
            </div>
          </section>
          <br></br>
          <YouTube videoId={video_id} opts={YoutubeOpts} onReady={this.videoOnReady} />
          <br></br>
          <section class="location_info_table">
            <Container className="table_container">
              <Table bordered className="table_country">
                <thead>
                  <tr>
                    <th> Category </th>
                    <th> Data </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td> IOC Code </td>
                    <td> {ioc_code} </td>
                  </tr>
                  <tr>
                    <td> Capital </td>
                    <td> {capital} </td>
                  </tr>
                  <tr>
                    <td> Languages </td>
                    <td>
                      <Button
                        color="dark grey"
                        id="languages_toggler"
                        style={{ marginBottom: "1rem" }}
                      >
                        View All
                      </Button>
                      <UncontrolledCollapse toggler="#languages_toggler">
                        {// conditional rendering, considering we need for the async callback
                        languages && (
                          <ListGroup>
                            {languages.map(single_language => (
                              <ListGroupItem>{single_language}</ListGroupItem>
                            ))}
                          </ListGroup>
                        )}
                      </UncontrolledCollapse>
                    </td>
                  </tr>
                  <tr>
                    <td> Region </td>
                    <td> {region} </td>
                  </tr>
                  <tr>
                    <td> Population </td>
                    <td>
                      {" "}
                      {population
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}{" "}
                    </td>
                  </tr>
                  <tr>
                    <td> GDP </td>
                    <td> {gdp} </td>
                  </tr>
                  <tr>
                    <td> Medal Count </td>
                    <td>
                      {" "}
                      {medal_count
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}{" "}
                    </td>
                  </tr>
                  <tr>
                    <td> Olympic Games Participated In </td>
                    {locations_info != null && locations_info.length > 0 ? (
                      <td>
                        <Button
                          color="dark grey"
                          id="locations_toggler"
                          style={{ marginBottom: "1rem" }}
                        >
                          View All
                        </Button>
                        <UncontrolledCollapse
                          className="view_all_drop_down"
                          toggler="#locations_toggler"
                        >
                          {// conditional rendering, considering we need for the async callback
                          locations_info && (
                            <ListGroup>
                              {locations_info.map(single_location => (
                                <ListGroupItem>
                                  <Link to={"/locations/" + single_location[1]}>
                                    {single_location[0]}
                                  </Link>
                                </ListGroupItem>
                              ))}
                            </ListGroup>
                          )}
                        </UncontrolledCollapse>
                      </td>
                    ) : (
                      <td> None </td>
                    )}
                  </tr>
                  <tr>
                    <td> Events Participated In </td>
                    {ep_info != null && ep_info.length > 0 ? (
                      <td>
                        <Button
                          color="dark grey"
                          id="ep_toggler"
                          style={{ marginBottom: "1rem" }}
                        >
                          View All
                        </Button>
                        <UncontrolledCollapse
                          className="view_all_drop_down"
                          toggler="#ep_toggler"
                        >
                          {// conditional rendering, considering we need for the async callback
                          ep_info && (
                            <ListGroup>
                              {ep_info.map(single_event => (
                                <ListGroupItem>
                                  <Link to={"/events/" + single_event[1]}>
                                    {single_event[0]}
                                  </Link>
                                </ListGroupItem>
                              ))}
                            </ListGroup>
                          )}
                        </UncontrolledCollapse>
                      </td>
                    ) : (
                      <td> None </td>
                    )}
                  </tr>
                  <tr>
                    <td> Events Medaled In </td>
                    {em_info != null && em_info.length > 0 ? (
                      <td>
                        <Button
                          color="dark grey"
                          id="em_toggler"
                          style={{ marginBottom: "1rem" }}
                        >
                          View All
                        </Button>
                        <UncontrolledCollapse
                          className="view_all_drop_down"
                          toggler="#em_toggler"
                        >
                          {// conditional rendering, considering we need for the async callback
                          em_info && (
                            <ListGroup>
                              {em_info.map(single_event => (
                                <ListGroupItem>
                                  <Link to={"/events/" + single_event[1]}>
                                    {single_event[0]}
                                  </Link>
                                </ListGroupItem>
                              ))}
                            </ListGroup>
                          )}
                        </UncontrolledCollapse>
                      </td>
                    ) : (
                      <td> None </td>
                    )}
                  </tr>
                  <tr>
                    <td> Events with Top Performer </td>
                    {eta_info != null && eta_info.length > 0 ? (
                      <td>
                        <Button
                          color="dark grey"
                          id="eta_toggler"
                          style={{ marginBottom: "1rem" }}
                        >
                          View All
                        </Button>
                        <UncontrolledCollapse
                          className="view_all_drop_down"
                          toggler="#eta_toggler"
                        >
                          {// conditional rendering, considering we need for the async callback
                          eta_info && (
                            <ListGroup>
                              {eta_info.map(single_event => (
                                <ListGroupItem>
                                  <Link to={"/events/" + single_event[1]}>
                                    {single_event[0]}
                                  </Link>
                                </ListGroupItem>
                              ))}
                            </ListGroup>
                          )}
                        </UncontrolledCollapse>
                      </td>
                    ) : (
                      <td> None </td>
                    )}
                  </tr>
                  <tr>
                    <td> Events with Most Recent Champion </td>
                    {elc_info != null && elc_info.length > 0 ? (
                      <td>
                        <Button
                          color="dark grey"
                          id="elc_toggler"
                          style={{ marginBottom: "1rem" }}
                        >
                          View All
                        </Button>
                        <UncontrolledCollapse
                          className="view_all_drop_down"
                          toggler="#elc_toggler"
                        >
                          {// conditional rendering, considering we need for the async callback
                          elc_info && (
                            <ListGroup>
                              {elc_info.map(single_event => (
                                <ListGroupItem>
                                  <Link to={"/events/" + single_event[1]}>
                                    {single_event[0]}
                                  </Link>
                                </ListGroupItem>
                              ))}
                            </ListGroup>
                          )}
                        </UncontrolledCollapse>
                      </td>
                    ) : (
                      <td> None </td>
                    )}
                  </tr>
                  <tr>
                    <td>Read more at Wikipedia</td>
                    <td>
                      <WikipediaLink search={display_name} />
                    </td>
                  </tr>
                </tbody>
              </Table>
            </Container>
          </section>
          <Link to="/countries" className="backLink">
            Back to Countries
          </Link>
        </main>
      );
    }
  }
  videoOnReady(event) {
	  event.target.pauseVideo()
	  console.log(event.target)
  }
}
export default Country;
