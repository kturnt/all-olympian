import React, {Component} from 'react'
import { Jumbotron, Container, Carousel, CarouselItem, CarouselCaption, CarouselControl, CarouselIndicators, UncontrolledCollapse, Modal, ModalBody, ModalHeader } from 'reactstrap'
import { Button } from 'reactstrap'
import '../styles/Home.css';

import eventsImg from '../images/events_slide.jpg';
import countriesImg from '../images/countries_slide.jpg';
import locationsImg from '../images/locations_slide.jpg';

import bballImg from '../images/basketball_slide.jpg';
import gbrImg from '../images/gbr_slide.jpg';
import pchngImg from '../images/pyeongchang_slide.jpg';

const items = [
	{
	  	src: locationsImg,
	  	altText: 'N/A',
	  	caption: 'Locations',
	  	href: '/locations',
	  	key: '5'
	},
	{
		src: pchngImg,
		altText: 'N/A',
		caption: 'Pyeongchang 2018',
		href: '/locations/cf7695f5-0a77-4451-a8f9-57ac6a3c7e19',
		key: '6'
	},
	{
		src: countriesImg,
		altText: 'N/A',
		caption: 'Countries',
		href: '/countries',
		key: '1'
	},
	{
		src: gbrImg,
		altText: 'N/A',
		caption: 'The United Kingdom',
		href: '/countries/GBR',
		key: '2'
	},
 	{
		src: eventsImg,
		altText: 'N/A',
		caption: 'Events',
		href: '/events',
		key: '3'
 	},
	{
		src: bballImg,
		altText: 'N/A',
		caption: 'Men\'s Basketball',
		href: '/events/706f875d-0a5a-4457-804d-4a77ad5eb6f4',
		key: '4'
	}
  ];

class Home extends Component {
	constructor(props) {
		super(props);
		this.state = { activeIndex: 0 };
		this.next = this.next.bind(this);
		this.previous = this.previous.bind(this);
		this.goToIndex = this.goToIndex.bind(this);
		this.onExiting = this.onExiting.bind(this);
		this.onExited = this.onExited.bind(this);
	}

	onExiting() {
		this.animating = true;
	}

	onExited() {
		this.animating = false;
	}

	next() {
		if (this.animating) return;
		const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
		this.setState({ activeIndex: nextIndex });
	}

	previous() {
		if (this.animating) return;
		const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
		this.setState({ activeIndex: nextIndex });
	}

	goToIndex(newIndex) {
		if (this.animating) return;
		this.setState({ activeIndex: newIndex });
	}


	render () {
		const { activeIndex } = this.state;

		const slides = items.map((item) => {
			return (
				<CarouselItem
					onExiting={this.onExiting}
					onExited={this.onExited}
					key={item.key}
				>
					<img src={item.src} alt={item.altText} />
					<CarouselCaption captionText={<Button color='primary' href={item.href}>Discover {item.caption}</Button>}></CarouselCaption>
				</CarouselItem>
			);
		});

		return (
			<main>
					<Jumbotron fluid className="my-jumbotron">
						<Container fluid>
							<h1 className="home_header">ALL OLYMPIAN</h1>
							<p className="home_paragraph">Discover more about the Olympics, an ancient celebration of physical fitness, athleticism, and competition.</p>
							<p className="home_paragraph">Go for gold and learn about various Locations of the Games as well as the many different competing Countries <br></br>
														and the types of Events.  As a whole, the Olympics are more than just a competition for medals; it's about a multicultural, <br></br> 
														ancient celebration of sporting events where its participants, the countries of the world, can put their differences aside <br></br>
														and engage in the festivities that the Games provide. </p>
							<Button color="primary" id="carousel_toggler" className="explore-btn">
								Explore
							</Button>
							<UncontrolledCollapse toggler="#carousel_toggler">
								<Carousel className="splash-carousel"
									activeIndex={activeIndex}
									next={this.next}
									previous={this.previous}
								>
									<CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
									{slides}
									<CarouselControl direction='prev' directionText='Previous' onClickHandler={this.previous} />
									<CarouselControl direction='next' directionText='Next' onClickHandler={this.next} />
								</Carousel>
							</UncontrolledCollapse>
						</Container>
					</Jumbotron>
			</main>
				);
	}
}

export default Home;
