import React from "react";
import YouTube from 'react-youtube'
import GitLab from "../components/GitLab";
import {
  Card,
  CardHeader,
  CardImg,
  CardFooter,
  CardText,
  CardGroup,
  Jumbotron,
  CardBody,
  CardLink
} from "reactstrap";
import { YoutubeURL, YoutubeOpts } from '../Youtube'

import reactLogo from "../images/react.png";
import dockerLogo from "../images/docker.png";
import awsLogo from "../images/aws.png";
import postmanLogo from "../images/postman.png";
import postgresLogo from "../images/postgres.jpg";
import flaskLogo from "../images/flask.png";
import sqlAlchemyLogo from "../images/sqlalchemy.jpg";
import seleniumLogo from "../images/selenium-logo.png";
import mochaLogo from "../images/mocha.png";
import pgAdminLogo from "../images/pgadmin.png";
import bsLogo from "../images/bs.png";
import pumlLogo from "../images/puml.jpg";

import "../styles/About.css";

function About() {
  return (
    <main>
      <Jumbotron className="about-jumbotron"></Jumbotron>
      <section className="about-intro">
        <h1>About Us</h1>
        <p>
          CS 373 Software Engineering Project by University of Texas at Austin
          students designed to increase public interest and pride in the Olympic
          Games by making data more accessible and portraying multimedia. We
          hope that visitors of our site will leave with a brand new curiosity
          and eagerness to research more Olympic informationon their own time,
          for this ancient celebration is an incredible contribution to global
          culture, traditions, and sports.
        </p>
      </section>
      <section className="team-info">
        <h1>The Team</h1>
        <GitLab />
      </section>
      <section className="tools">
        <h1>Tools</h1>
        <CardGroup className="tools-card-group">
          <Card className="tools-card">
            <CardHeader className="tools-card-header">
              <h3>ReactJS</h3>
            </CardHeader>
            <CardImg src={reactLogo} className="tools-card-img" />
            <CardBody>
              Frontend framework for rendering pages and elements.
            </CardBody>
          </Card>
          <Card className="tools-card">
            <CardHeader className="tools-card-header">
              <h3>Docker</h3>
            </CardHeader>
            <CardImg src={dockerLogo} className="tools-card-img" />
            <CardBody>
              For providing a container to deploy frontend and backend
              environments.
            </CardBody>
          </Card>
          <Card className="tools-card">
            <CardHeader className="tools-card-header">
              <h3>Amazon Web Services</h3>
            </CardHeader>
            <CardImg src={awsLogo} className="tools-card-img" />
            <CardBody>
              Hosting the frontend, backend and DB environments
            </CardBody>
          </Card>
          <Card className="tools-card">
            <CardHeader className="tools-card-header">
              <h3>Postman</h3>
            </CardHeader>
            <CardImg className="tools-card-img" src={postmanLogo} />
            <CardBody>Used to define our API</CardBody>
          </Card>
          <Card className="tools-card">
            <CardHeader className="tools-card-header">
              <h3>Mocha</h3>
            </CardHeader>
            <CardImg src={mochaLogo} className="tools-card-img" />
            <CardBody>Unit testing the frontend</CardBody>
          </Card>
          <Card className="tools-card">
            <CardHeader className="tools-card-header">
              <h3>PlantUML</h3>
            </CardHeader>
            <CardImg src={pumlLogo} className="tools-card-img" />
            <CardBody>For UML creation</CardBody>
          </Card>
        </CardGroup>

        <CardGroup className="tools-card-group">
          <Card className="tools-card">
            <CardHeader className="tools-card-header">
              <h3>PostgreSQL</h3>
            </CardHeader>
            <CardImg src={postgresLogo} className="tools-card-img" />
            <CardBody>Relational database for our models</CardBody>
          </Card>
          <Card className="tools-card">
            <CardHeader className="tools-card-header">
              <h3>Flask</h3>
            </CardHeader>
            <CardImg src={flaskLogo} className="tools-card-img" />
            <CardBody>
              Backend framework for defining RESTful API for data
            </CardBody>
          </Card>
          <Card className="tools-card">
            <CardHeader className="tools-card-header">
              <h3>SQLAlchemy</h3>
            </CardHeader>
            <CardImg src={sqlAlchemyLogo} className="tools-card-img" />
            <CardBody>Python module for ORM with relational database.</CardBody>
          </Card>
          <Card className="tools-card">
            <CardHeader className="tools-card-header">
              <h3>Selenium</h3>
            </CardHeader>
            <CardImg src={seleniumLogo} className="tools-card-img" />
            <CardBody>For acceptance tests on the frontend</CardBody>
          </Card>
          <Card className="tools-card">
            <CardHeader className="tools-card-header">
              <h3>pgAdmin</h3>
            </CardHeader>
            <CardImg src={pgAdminLogo} className="tools-card-img" />
            <CardBody>For defining and viewing database tables</CardBody>
          </Card>
          <Card className="tools-card">
            <CardHeader className="tools-card-header">
              <h3>Beautiful Soup</h3>
            </CardHeader>
            <CardImg src={bsLogo} className="tools-card-img" />
            <CardBody>For collecting data for models</CardBody>
          </Card>
        </CardGroup>
      </section>

      <section className="data">
        <h1>Data</h1>
        <CardGroup className="data-card-group">
          <Card>
            <CardHeader>
              <h5>Olympics Data</h5>
            </CardHeader>
            <CardFooter>
              <CardLink href="https://www.olympic.org/sports">
                International Olympic Committee
              </CardLink>
            </CardFooter>
            <CardFooter>
              <CardLink href="https://www.kaggle.com/heesoo37/120-years-of-olympic-history-athletes-and-results#athlete_events.csv">
                Olympic History
              </CardLink>
            </CardFooter>
          </Card>

          <Card>
            <CardHeader>
              <h5>Countries Data</h5>
            </CardHeader>
            <CardFooter>
              <CardLink href="https://github.com/iancoleman/cia_world_factbook_api">
                CIA World Factbook
              </CardLink>
            </CardFooter>
            <CardFooter>
              <CardLink href="https://datahelpdesk.worldbank.org/knowledgebase/articles/898590-country-api-queries">
                World Bank Country API
              </CardLink>
            </CardFooter>
          </Card>

          <Card>
            <CardHeader>
              <h5>Media</h5>
            </CardHeader>
            <CardFooter>
              <CardLink href="https://developers.google.com/youtube/v3/docs/search/list">
                Youtube
              </CardLink>
            </CardFooter>
            <CardFooter>
              <CardLink href="https://azure.microsoft.com/en-us/services/cognitive-services/bing-image-search-api/">
                Bing Search API
              </CardLink>
            </CardFooter>
          </Card>
        </CardGroup>
      </section>

      <section className="development">
        <h1>Development</h1>
        <CardGroup className="development-card-group">
          <Card>
            <CardHeader>
              <CardLink href="https://gitlab.com/kturnt/all-olympian">
                All Olympian Gitlab Repository
              </CardLink>
            </CardHeader>
            <CardFooter>
              <CardLink href="https://documenter.getpostman.com/view/10505725/SzKYPc55?version=latest">
                All Olympian Postman API
              </CardLink>
            </CardFooter>
          </Card>
        </CardGroup>
        <br></br>
        <h1>Video Presentation</h1>
        <YouTube videoId="pHfb_RzARMo" opts={YoutubeOpts} onReady={videoOnReady} />
      </section>
    </main>
  );
}

function videoOnReady(event) {
  event.target.pauseVideo()
  console.log(event.target)
}

export default About;
