import React, { Component } from "react";
import YouTube from 'react-youtube'
import { Link } from "react-router-dom";
import { Table, Container, Spinner } from "reactstrap";
import "../styles/SingleEvent.css";
import { YoutubeURL, YoutubeOpts } from '../Youtube'
import WikipediaLink from "../components/WikipediaLink";

class Event extends Component {
  constructor(props) {
    super(props);
    this.state = {
      event_name: null,
      overarching_sport: null,
      season: null,
      gender: null,
      id: null,
      debut_location: null,
      dl_info: null,
      debut_year: null,
      image_url: null,
      latest_winner: null,
      latest_winner_country: null,
      top_athlete: null,
      top_athlete_country: null,
      isLoaded: false
    };
  }

  async componentDidMount() {
    const event_id = this.props.match.params.eventId;
    console.log("EVENT ID IS" + event_id);

    const url = "https://api.all-olympian.com//api/event?id=" + event_id;
    console.log("THE URL IS" + url);
    await fetch(url)
      .then(res => res.json())
      .then(data => {
        this.setState({
          event_name: data.event,
          overarching_sport: data.overarching_sport,
          season: data.season,
          gender:
            data.event.toLowerCase().includes("women") ||
            data.event.toLowerCase().includes("ladies")
              ? "Female"
              : data.gender,
          id: data.id,
          debut_location: data.debut_location,
          debut_year: data.debut_year,
          image_url: data.image_url,
          latest_winner: data.latest_champion,
          latest_winner_country: data.latest_champion_country,
          top_athlete: data.top_athlete,
          top_athlete_country: data.top_athlete_country,
          isLoaded: true
        });
        console.log("DATA LOADED FROM DB");
      })
      .then(() => {
        //LOCATIONS PARTICIPATED
        const baseurl =
          "https://ja1sq0kcre.execute-api.us-west-2.amazonaws.com/api/olympics?ids=";
        var toappend = "";
        console.log("trying to get location now");
        toappend += this.state.debut_location;
        const tofetch = baseurl + toappend;
        fetch(tofetch)
          .then(location => location.json())
          .then(location_data => {
            var tempList = [];
            var tempLoc = location_data[0].city + " " + location_data[0].year;
            var tempLink = location_data[0].id;
            tempList.push(tempLoc);
            tempList.push(tempLink);
            this.setState({
              dl_info: tempList
            });
            console.log("fetching from API 2 DONE");
          });
      });
    var query = this.state.overarching_sport + " " + this.state.event_name + " "
    if(this.state.season == "summer") {
	    query += "rio 2016"
    } else {
	    query += "pyeongchang 2018"
    }
    const youtubeUrl = YoutubeURL + "&q=" + query
    await fetch(youtubeUrl)
    .then(res => res.json())
    .then(
	    (data) => {
		    this.setState({
			    video_id: data.items[0].id.videoId
		    });
		    console.log("SEARCHED FOR VIDEO ID " + this.state.video_id)
	    }
    )
  }

  render() {
    const {
      event_name,
      overarching_sport,
      season,
      gender,
      id,
      debut_location,
      dl_info,
      debut_year,
      image_url,
      latest_winner,
      latest_winner_country,
      top_athlete,
      top_athlete_country,
      isLoaded,
      video_id
    } = this.state;

    console.log("VARIABLES NEED TO BE LOADED");
    console.log("IMAGE_URL" + image_url);
    console.log("STATE" + this.state.event_name);
    console.log("ISLOADED" + isLoaded);
    console.log("SEASON " + season);

    if (!isLoaded) {
      return (
        <div>
          LOADING...
          <Spinner type="grow" color="primary" />
        </div>
      );
    } else {
      return (
        <main>
          <section class="event_header">
            <div class="holder">
              <h1 class="event_name">{event_name}</h1>
            </div>
          </section>
          <br></br>
          <section className="event_image_section">
            <div class="holder">
              <img class="event_image" src={image_url}></img>
            </div>
          </section>
          <br></br>
          <YouTube videoId={video_id} opts={YoutubeOpts} onReady={this.videoOnReady} />
					<br></br>
          <section class="event_info_table">
            <Container className="table_container">
              <Table bordered className="table_event">
                <thead>
                  <tr>
                    <th> Category </th>
                    <th> Data </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td> Sport </td>
                    <td> {overarching_sport} </td>
                  </tr>
                  <tr>
                    <td> Gender </td>
                    <td>
                      {" "}
                      {gender.charAt(0).toUpperCase() + gender.slice(1)}{" "}
                    </td>
                  </tr>
                  <tr>
                    <td> Season </td>
                    <td>
                      {" "}
                      {season == "winter" ? (
                        <Link to="/locations/cf7695f5-0a77-4451-a8f9-57ac6a3c7e19">
                          {" "}
                          Winter ❄️{" "}
                        </Link>
                      ) : (
                        <Link to="/locations/c34b0ff1-ac19-4a48-884a-39bf8336f3ba">
                          {" "}
                          Summer ☀️{" "}
                        </Link>
                      )}{" "}
                    </td>
                  </tr>
                  {dl_info != null &&
                  dl_info[0] != null &&
                  dl_info[1] != null ? (
                    <tr>
                      <td> Debuted at </td>
                      <td>
                        {" "}
                        <Link to={"/locations/" + dl_info[1]}>
                          {" "}
                          {dl_info[0]}{" "}
                        </Link>{" "}
                      </td>
                    </tr>
                  ) : (
                    ""
                  )}
                  <tr>
                    <td> Latest Winner </td>
                    <td>
                      {" "}
                      {latest_winner},{" "}
                      {latest_winner_country != "N/A" ? (
                        <Link to={"/countries/" + latest_winner_country}>
                          {" "}
                          {latest_winner_country}{" "}
                        </Link>
                      ) : (
                        latest_winner_country
                      )}{" "}
                    </td>
                  </tr>
                  <tr>
                    <td> Top Athlete </td>
                    <td>
                      {" "}
                      {top_athlete},{" "}
                      {top_athlete_country != "N/A" ? (
                        <Link to={"/countries/" + top_athlete_country}>
                          {" "}
                          {top_athlete_country}{" "}
                        </Link>
                      ) : (
                        top_athlete_country
                      )}{" "}
                    </td>
                  </tr>
                  <tr>
                    <td>Read more at Wikipedia</td>
                    <td>
                      <WikipediaLink search={overarching_sport} />
                    </td>
                  </tr>
                </tbody>
              </Table>
            </Container>
          </section>
          <Link to="/events" className="backLink">
            Back to Events
          </Link>
        </main>
      );
    }
  }
  videoOnReady(event) {
	  event.target.pauseVideo()
	  console.log(event.target)
  }
}

export default Event;
