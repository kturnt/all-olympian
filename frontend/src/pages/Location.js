import React, { Component } from "react";
import YouTube from 'react-youtube'
import { Link } from "react-router-dom";
import {
  UncontrolledCollapse,
  Button,
  Table,
  Container,
  Spinner,
  ListGroup,
  ListGroupItem
} from "reactstrap";
import "../styles/SingleLocation.css";
import { YoutubeURL, YoutubeOpts } from '../Youtube'
import WikipediaLink from "../components/WikipediaLink";

class Location extends Component {
  constructor(props) {
    super(props);
    this.state = {
      city: null,
      hosting_country: null,
      host_code: null,
      year: null,
      season: null,
      id: null,
      opening_ceremony_url: null,
      all_events: null,
      ae_info: null,
      logo_url: null,
      num_athletes: null,
      num_events: null,
      num_participating_countries: null,
      top_athlete: null,
      top_athlete_country: null,
      isLoaded: false
    };
  }

  async componentDidMount() {
    const location_id = this.props.match.params.locationId;

    const url = "https://api.all-olympian.com//api/olympics?ids=" + location_id;
    console.log(url);
    await fetch(url)
      .then(res => res.json())
      .then(data => {
        this.setState({
          city: data[0].city,
          hosting_country: data[0].hosting_country,
          host_code: data[0].host_code,
          year: data[0].year,
          season: data[0].season,
          id: data[0].id,
          opening_ceremony_url: data[0].opening_ceremony_url,
          all_events: data[0].all_events,
          logo_url: data[0].logo_url,
          num_athletes: data[0].num_athletes,
          num_events: data[0].num_events,
          num_participating_countries: data[0].num_participating_countries,
          top_athlete: data[0].top_athlete,
          top_athlete_country: data[0].top_athlete_country,
          isLoaded: true
        });
        console.log("DATA LOADED FROM DB");
        console.log("state" + this.state.city);
      })
      .then(() => {
        //Events put on
        const baseurl =
          "https://ja1sq0kcre.execute-api.us-west-2.amazonaws.com/api/events?ids=";
        var toappend = "";
        console.log("trying to get events put on now");
        var theMax = this.state.all_events.length;
        for (var i = 0; i < theMax - 1; i++) {
          toappend += this.state.all_events[i] + ",";
        }
        toappend += this.state.all_events[theMax - 1];
        const tofetch = baseurl + toappend;
        fetch(tofetch)
          .then(event => event.json())
          .then(event_data => {
            var tempList = [];
            for (var j = 0; j < event_data.length; j++) {
              var toAdd = [];
              var tempEvent =
                event_data[j].overarching_sport +
                " " +
                event_data[j].event +
                " (" +
                event_data[j].gender +
                ")";
              var tempLink = event_data[j].id;
              toAdd.push(tempEvent);
              toAdd.push(tempLink);
              tempList.push(toAdd);
            }
            this.setState({
              ae_info: tempList
            });
            console.log("fetching from API 2 DONE");
          });
      });
    const query = this.state.city + " " + this.state.year + " " + this.state.season + " olympics opening ceremony"
    const youtubeUrl = YoutubeURL + "&q=" + query
    await fetch(youtubeUrl)
    .then(res => res.json())
    .then(
	    (data) => {
		    this.setState({
			    video_id: data.items[0].id.videoId
		    });
		    console.log("SEARCHED FOR VIDEO ID " + this.state.video_id)
	    }
    )
  }

  render() {
    const {
      city,
      hosting_country,
      host_code,
      year,
      season,
      id,
      opening_ceremony_url,
      all_events,
      ae_info,
      logo_url,
      num_athletes,
      num_events,
      num_participating_countries,
      top_athlete,
      top_athlete_country,
      isLoaded,
      video_id
    } = this.state;

    console.log("IN RENDER");
    console.log("LOCAL VARS = " + city);
    console.log("isLoaded LOCAL VAR? + " + isLoaded);
    console.log("isLoaded state? + " + this.state.isLoaded);
    console.log("STATE = " + this.state.city);
    console.log("State = " + this.state);

    if (!isLoaded) {
      return (
        <div>
          LOADING...
          <Spinner type="grow" color="primary" />
        </div>
      );
    } else {
      return (
        <main>
          <section class="location_header">
            <div class="holder">
              <h1 class="location_name">
                {city} {year}
              </h1>
            </div>
          </section>
          <br></br>
          <section className="location_image_section">
            <div class="holder">
              <img class="location_image" src={opening_ceremony_url}></img>
            </div>
          </section>
          <br></br>
          <YouTube videoId={video_id} opts={YoutubeOpts} onReady={this.videoOnReady} />
					<br></br>
          <section class="location_info_table">
            <Container className="table_container">
              <Table bordered className="table_location">
                <thead>
                  <tr>
                    <th> Category </th>
                    <th> Data </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td> City </td>
                    <td> {city} </td>
                  </tr>
                  <tr>
                    <td> Hosting Country </td>
                    <td>
                      {" "}
                      {hosting_country}{" "}
                      {host_code != null ? (
                        <Link to={"/countries/" + host_code}>
                          ({host_code}){" "}
                        </Link>
                      ) : (
                        ""
                      )}{" "}
                    </td>
                  </tr>
                  <tr>
                    <td> Season </td>
                    <td> {season == "winter" ? "Winter ❄️" : "Summer ☀️"}</td>
                  </tr>
                  <tr>
                    <td> Number of Athletes </td>
                    <td>
                      {" "}
                      {num_athletes
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}{" "}
                    </td>
                  </tr>
                  <tr>
                    <td> Number of Events </td>
                    <td>
                      {" "}
                      {num_events
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}{" "}
                    </td>
                  </tr>
                  <tr>
                    <td> Number of Participating Countries </td>
                    <td>
                      {" "}
                      {num_participating_countries
                        .toString()
                        .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}{" "}
                    </td>
                  </tr>
                  <tr>
                    <td> Events Put On </td>
                    {ae_info != null && ae_info.length > 0 ? (
                      <td>
                        <Button
                          color="dark grey"
                          id="locations_toggler"
                          style={{ marginBottom: "1rem" }}
                        >
                          View All
                        </Button>
                        <UncontrolledCollapse
                          className="view_all_drop_down"
                          toggler="#locations_toggler"
                        >
                          {// conditional rendering, considering we need for the async callback
                          ae_info && (
                            <ListGroup>
                              {ae_info.map(single_event => (
                                <ListGroupItem>
                                  <Link to={"/events/" + single_event[1]}>
                                    {single_event[0]}
                                  </Link>
                                </ListGroupItem>
                              ))}
                            </ListGroup>
                          )}
                        </UncontrolledCollapse>
                      </td>
                    ) : (
                      <td>None</td>
                    )}
                  </tr>
                  <tr>
                    <td> Top Athlete </td>
                    <td>
                      {" "}
                      {top_athlete},{" "}
                      {top_athlete_country != "N/A" ? (
                        <Link to={"/countries/" + top_athlete_country}>
                          {" "}
                          {top_athlete_country}{" "}
                        </Link>
                      ) : (
                        top_athlete_country
                      )}{" "}
                    </td>
                  </tr>
                  <tr>
                    <td>Read more at Wikipedia</td>
                    <td>
                      <WikipediaLink search={city + " " + year} />
                    </td>
                  </tr>
                </tbody>
              </Table>
            </Container>
          </section>
          <Link to="/locations" className="backLink">
            Back to Locations
          </Link>
        </main>
      );
    }
  }
  videoOnReady(event) {
	  event.target.pauseVideo()
	  console.log(event.target)
  }
}

export default Location;
