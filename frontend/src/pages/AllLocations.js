import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Card,
  Button,
  CardImg,
  CardTitle,
  CardText,
  CardDeck,
  CardBody,
  Jumbotron,
} from "reactstrap";
import "../styles/CardDeckView.css";
import "../styles/Footer.css";
import AllPagination from "../components/AllPagination.js";
import AllLocationsToolBar from "../components/AllLocationsToolBar.js";
import LocationsCompareCard from "../components/LocationsCompareCard.js"
import CompareModal from '../components/CompareModal';

const numPerPage = 9;
const numPerRow = 3;

class AllLocations extends Component {
  constructor(props) {
    super(props);
    // search
    this.setSearchTerm = this.setSearchTerm.bind(this);
    this.searchLocations = this.searchLocations.bind(this);
    this.displayContains = this.displayContains.bind(this);
    // season filter
    this.toggleSeason = this.toggleSeason.bind(this);
    this.changeSeason = this.changeSeason.bind(this);
    // filter number of athletes
    this.toggleNumAthletes = this.toggleNumAthletes.bind(this);
    this.setMinNumAthletes = this.setMinNumAthletes.bind(this);
    this.setMaxNumAthletes = this.setMaxNumAthletes.bind(this);
    this.changeNumAthletes = this.changeNumAthletes.bind(this);
    // filter number of events
    this.toggleNumEvents = this.toggleNumEvents.bind(this);
    this.setMinNumEvents = this.setMinNumEvents.bind(this);
    this.setMaxNumEvents = this.setMaxNumEvents.bind(this);
    this.changeNumEvents = this.changeNumEvents.bind(this);
    // filter number of countries
    this.toggleNumCountries = this.toggleNumCountries.bind(this);
    this.setMinNumCountries = this.setMinNumCountries.bind(this);
    this.setMaxNumCountries = this.setMaxNumCountries.bind(this);
    this.changeNumCountries = this.changeNumCountries.bind(this);
    // apply all filters
    this.filterLocations = this.filterLocations.bind(this);
    this.sortData = this.sortData.bind(this);
    this.godSearch = this.godSearch.bind(this);
    // reset all filters
    this.resetFilters = this.resetFilters.bind(this);
    //sort
    this.toggleSort = this.toggleSort.bind(this);
    this.changeSort = this.changeSort.bind(this);
    this.toggleAsc = this.toggleAsc.bind(this);
    this.changeAsc = this.changeAsc.bind(this);
    //compare 
    this.compareChange = this.compareChange.bind(this);
    //state
    this.state = {
      page: 1,
      allLocationsData: null,
      displayedLocationsData: null,
      isLoaded: false,
      seasonDropDownOpen: false,
      seasonDropDown: "Season",
      numAthletesDropDownOpen: false,
      numAthletesDropDown: "# of Athletes",
      numAthletesMin: 0,
      numAthletesMax: Number.MAX_SAFE_INTEGER,
      numEventsDropDownOpen: false,
      numEventsDropDown: "# of Events",
      numEventsMin: 0,
      numEventsMax: Number.MAX_SAFE_INTEGER,
      numCountriesDropDownOpen: false,
      numCountriesDropDown: "# of Countries",
      numCountriesMin: 0,
      numCountriesMax: Number.MAX_SAFE_INTEGER,
      sortDropDownOpen: false,
      sortDropDown: "Attribute",
      ascDropDownOpen: false,
      ascDropDown: "Asc.",
      searchTerm: "",
      compareGroup: []
    };
  }

  toggleAsc(event) {
    this.setState({
      ascDropDownOpen: !this.state.ascDropDownOpen
    });
  }

  changeAsc(e) {
    this.setState({ ascDropDown: e.currentTarget.textContent });
    let asc = e.currentTarget.innerText;
    console.log(asc);
  }

  toggleSort(event) {
    this.setState({
      sortDropDownOpen: !this.state.sortDropDownOpen
    });
  }

  changeSort(e) {
    this.setState({ sortDropDown: e.currentTarget.textContent });
    let sortCrit = e.currentTarget.innerText;
    console.log(sortCrit);
  }

  cityCompare(ea, eb) {
    return ea.city.localeCompare(eb.city);
  }

  yearCompare(ea, eb) {
    return ea.year - eb.year;
  }

  countryCompare(ea, eb) {
    return ea.hosting_country.localeCompare(eb.hosting_country);
  }

  seasonCompare(ea, eb) {
    return ea.season.localeCompare(eb.season);
  }

  athleteCompare(ea, eb) {
    return ea.num_athletes - eb.num_athletes;
  }

  eventsCompare(ea, eb) {
    return ea.num_events - eb.num_events;
  }

  countriesCompare(ea, eb) {
    return ea.num_participating_countries - eb.num_participating_countries;
  }

  sortData() {
    var didChange = false;
    var safeToSort = Array.from(this.state.displayedLocationsData);
    if (this.state.sortDropDown === "Location City") {
      safeToSort = safeToSort.sort(this.cityCompare);
      didChange = true;
    } else if (this.state.sortDropDown === "Year") {
      safeToSort = safeToSort.sort(this.yearCompare);
      didChange = true;
    } else if (this.state.sortDropDown === "Hosting Country") {
      safeToSort = safeToSort.sort(this.countryCompare);
      didChange = true;
    } else if (this.state.sortDropDown === "Season") {
      safeToSort = safeToSort.sort(this.seasonCompare);
      didChange = true;
    } else if (this.state.sortDropDown === "Athletes") {
      safeToSort = safeToSort.sort(this.athleteCompare);
      didChange = true;
    } else if (this.state.sortDropDown === "Events") {
      safeToSort = safeToSort.sort(this.eventsCompare);
      didChange = true;
    } else if (this.state.sortDropDown === "Countries") {
      safeToSort = safeToSort.sort(this.countriesCompare);
      didChange = true;
    }

    // only refresh the page once all the filters (if any) have been applied
    if (didChange) {
      if (this.state.ascDropDown === "Desc.") safeToSort = safeToSort.reverse();
      this.state.displayedLocationsData = safeToSort;
      this.state.page = 1;
      this.forceUpdate();
    }
  }

  // Season Dropdown
  toggleSeason() {
    this.setState({
      seasonDropDownOpen: !this.state.seasonDropDownOpen
    });
  }

  changeSeason(e) {
    this.setState({ seasonDropDown: e.currentTarget.textContent });
    let season = e.currentTarget.innerText;
    console.log(season);
  }

  // # of Athletes Dropdown
  toggleNumAthletes() {
    this.setState({
      numAthletesDropDownOpen: !this.state.numAthletesDropDownOpen
    });
  }

  changeNumAthletes() {
    var numAthletes = "";
    if (this.state.numAthletesMax === Number.MAX_SAFE_INTEGER) {
      numAthletes = "# of Athletes: " + ">= " + this.state.numAthletesMin;
    } else {
      numAthletes =
        "# of Athletes: " +
        this.state.numAthletesMin +
        "-" +
        this.state.numAthletesMax;
    }
    this.setState({ numAthletesDropDown: numAthletes });
    console.log(numAthletes);
  }

  setMinNumAthletes(val) {
    this.setState({ numAthletesMin: val });
  }

  setMaxNumAthletes(val) {
    this.setState({ numAthletesMax: val });
  }

  // # of Events Dropdown
  toggleNumEvents() {
    this.setState({
      numEventsDropDownOpen: !this.state.numEventsDropDownOpen
    });
  }

  changeNumEvents() {
    var numEvents = "";
    if (this.state.numEventsMax === Number.MAX_SAFE_INTEGER) {
      numEvents = "# of Events: " + ">= " + this.state.numEventsMin;
    } else {
      numEvents =
        "# of Events: " +
        this.state.numEventsMin +
        "-" +
        this.state.numEventsMax;
    }
    this.setState({ numEventsDropDown: numEvents });
    console.log(numEvents);
  }

  setMinNumEvents(val) {
    this.setState({ numEventsMin: val });
  }

  setMaxNumEvents(val) {
    this.setState({ numEventsMax: val });
  }

  // # of Countries Dropdown
  toggleNumCountries() {
    this.setState({
      numCountriesDropDownOpen: !this.state.numCountriesDropDownOpen
    });
  }

  changeNumCountries() {
    var numCountries = "";
    if (this.state.numCountriesMax === Number.MAX_SAFE_INTEGER) {
      numCountries = "# of Countries: " + ">= " + this.state.numCountriesMin;
    } else {
      numCountries =
        "# of Countries: " +
        this.state.numCountriesMin +
        "-" +
        this.state.numCountriesMax;
    }
    this.setState({ numCountriesDropDown: numCountries });
    console.log(numCountries);
  }

  setMinNumCountries(val) {
    this.setState({ numCountriesMin: val });
  }

  setMaxNumCountries(val) {
    this.setState({ numCountriesMax: val });
  }

  setSearchTerm(val) {
    this.setState({ searchTerm: val });
    console.log(this.state.searchTerm);
  }

  async componentDidMount() {
    const url = "https://api.all-olympian.com/api/olympics"; //all locations
    await fetch(url)
      .then(res => res.json())
      .then(data => {
        this.setState({
          allLocationsData: data,
          displayedLocationsData: data,
          isLoaded: true
        });
      });
  }

  getPage = pageNum => {
    console.log("inside getPage()");
    this.setState({
      page: pageNum
    });
    return this.getLocations();
  };

  getLocations = (currPage, locationsInfo) => {
    if (!locationsInfo) {
      console.log("ERROR: LOCATIONS INFO NOT FETCHED CORRECTLY");
    }
    // current list of countries to display
    console.log("IN GET LOCATIONS");
    const correctPage = currPage - 1;
    var startIndex = correctPage * numPerPage;
    let endIndex = startIndex + numPerPage;
    const max = this.state.displayedLocationsData.length;
    if (endIndex > max) endIndex = max;
    let outer = [];
    let inner = [];
    while (startIndex < endIndex) {
      //need a new row
      var curLocation = locationsInfo[startIndex];
      inner.push(curLocation);
      if (inner.length % numPerRow == 0) {
        outer.push(inner);
        inner = [];
      }
      startIndex++;
    }
    if (inner.length > 0) {
      outer.push(inner);
    }
    return outer;
  };

  // displayContains
  displayContains = str => {
    if (str === "") return false;
    var splitSearch = this.state.searchTerm.split(/{|,|\.|\s}*/g);
    const toComp = str.toLowerCase();
    for (var i = 0; i < splitSearch.length; i++) {
      var curTerm = splitSearch[i].toLowerCase();
      if (curTerm === "") continue;
      else if (toComp.includes(curTerm)) {
        return true;
      }
    }
    return false;
  };

  filterLocations = () => {
    // temp var for determining if any filtration occurs
    var didChange = false;
    // season filter
    if (this.state.seasonDropDown != "Season") {
      console.log("season filter");
      console.log(this.state.seasonDropDown);
      this.state.displayedLocationsData = this.state.displayedLocationsData.filter(
        data => data.season == this.state.seasonDropDown.toLowerCase()
      );
      didChange = true;
    }
    // # of Athletes filter
    if (this.state.numAthletesDropDown != "# of Athletes") {
      this.state.displayedLocationsData = this.state.displayedLocationsData.filter(
        data =>
          data.num_athletes >= this.state.numAthletesMin &&
          data.num_athletes <= this.state.numAthletesMax
      );
      didChange = true;
    }
    // # of Events filter
    if (this.state.numEventsDropDown != "# of Events") {
      this.state.displayedLocationsData = this.state.displayedLocationsData.filter(
        data =>
          data.num_events >= this.state.numEventsMin &&
          data.num_events <= this.state.numEventsMax
      );
      didChange = true;
    }
    // # of Countries filter
    if (this.state.numCountriesDropDown != "# of Countries") {
      this.state.displayedLocationsData = this.state.displayedLocationsData.filter(
        data =>
          data.num_participating_countries >= this.state.numCountriesMin &&
          data.num_participating_countries <= this.state.numCountriesMax
      );
      didChange = true;
    }

    // only refresh the page once all the filters (if any) have been applied
    if (didChange) {
      this.state.page = 1;
      this.forceUpdate();
    }
  };

  // resets state variables to default values, used when clearing filters on data
  resetFilters = () => {
    this.state.displayedLocationsData = this.state.allLocationsData;
    this.state.page = 1;
    this.state.seasonDropDown = "Season";
    this.state.seasonDropDownOpen = false;
    this.state.numAthletesDropDown = "# of Athletes";
    this.state.numAthletesMin = 0;
    this.state.numAthletesMax = Number.MAX_SAFE_INTEGER;
    this.state.numEventsDropDown = "# of Events";
    this.state.numEventsMin = 0;
    this.state.numEventsMax = Number.MAX_SAFE_INTEGER;
    this.state.numCountriesDropDown = "# of Countries";
    this.state.numCountriesMin = 0;
    this.state.numCountriesMax = Number.MAX_SAFE_INTEGER;
    this.state.sortDropDownOpen = false;
    this.state.sortDropDown = "Attribute";
    this.state.ascDropDown = "Asc.";
    this.state.searchTerm = "";
    var searchBox = document.getElementById("localSearch");
    searchBox.value = "";
    this.state.compareGroup = [];
    this.forceUpdate();
  };

  searchLocations = () => {
    // tokenize our searchTerm on commas, dots, and white space
    // this.state.searchDisabled = true
    if (this.state.searchTerm == "") {
      return;
    }
    var splitSearch = this.state.searchTerm.split(/{|,|\.|\s}*/g);
    console.log("in searchEvents");
    console.log(splitSearch);
    var matchedLocations = [];
    for (var i = 0; i < this.state.displayedLocationsData.length; i++) {
      const curLocation = this.state.displayedLocationsData[i];
      for (var j = 0; j < splitSearch.length; j++) {
        var curTerm = splitSearch[j].toLowerCase();
        if (curTerm == "") {
          continue;
        } else {
          if (
            curLocation.city.toLowerCase().includes(curTerm) ||
            curLocation.year.toString().includes(curTerm) ||
            curLocation.hosting_country.toLowerCase().includes(curTerm) ||
            curLocation.season.toLowerCase().includes(curTerm) ||
            curLocation.top_athlete.toLowerCase().includes(curTerm) ||
            curLocation.top_athlete_country.toLowerCase().includes(curTerm)
          ) {
            if (!matchedLocations.includes(curLocation))
              matchedLocations.push(curLocation);
          }
        }
      }
    }
    this.state.displayedLocationsData = matchedLocations;
    this.state.page = 1;
    this.forceUpdate();
  };

  godSearch = () => {
    this.state.displayedLocationsData = this.state.allLocationsData;
    this.filterLocations();
    this.searchLocations();
    if (this.state.sortDropDown != "Attribute") {
      this.sortData();
    }
  };

  compareChange = (locationToChange) => {
    console.log("CALLED COMPARE CHANGE")
		console.log(this.state.compareGroup)
		if(this.state.compareGroup.includes(locationToChange)){
			console.log("IN COMPARE GROUP")
			const index = this.state.compareGroup.indexOf(locationToChange);
			this.state.compareGroup.splice(index, 1);
			console.log(this.state.compareGroup)
		}
		else{
			if(this.state.compareGroup.length >= 2)
				return;
			this.state.compareGroup.push(locationToChange)
		}
		this.forceUpdate()   
  }

  renderToolBar() {
    return (
      <AllLocationsToolBar
        // dropdown attributes
        seasonDropDownOpen={this.state.seasonDropDownOpen}
        seasonDropDown={this.state.seasonDropDown}
        numAthletesDropDownOpen={this.state.numAthletesDropDownOpen}
        numAthletesDropDown={this.state.numAthletesDropDown}
        numEventsDropDownOpen={this.state.numEventsDropDownOpen}
        numEventsDropDown={this.state.numEventsDropDown}
        numCountriesDropDownOpen={this.state.numCountriesDropDownOpen}
        numCountriesDropDown={this.state.numCountriesDropDown}
        sortDropDownOpen={this.state.sortDropDownOpen}
        sortDropDown={this.state.sortDropDown}
        ascDropDownOpen={this.state.ascDropDownOpen}
        ascDropDown={this.state.ascDropDown}
        // search
        setSearchTerm={this.setSearchTerm}
        searchLocations={this.searchLocations}
        // filter
        toggleSeason={this.toggleSeason}
        changeSeason={this.changeSeason}
        toggleNumAthletes={this.toggleNumAthletes}
        setMinNumAthletes={this.setMinNumAthletes}
        setMaxNumAthletes={this.setMaxNumAthletes}
        changeNumAthletes={this.changeNumAthletes}
        toggleNumEvents={this.toggleNumEvents}
        setMinNumEvents={this.setMinNumEvents}
        setMaxNumEvents={this.setMaxNumEvents}
        changeNumEvents={this.changeNumEvents}
        toggleNumCountries={this.toggleNumCountries}
        setMinNumCountries={this.setMinNumCountries}
        setMaxNumCountries={this.setMaxNumCountries}
        changeNumCountries={this.changeNumCountries}
        // apply filters
        filterLocations={this.filterLocations}
        // reset filters
        resetFilters={this.resetFilters}
        // sort
        toggleSort={this.toggleSort}
        changeSort={this.changeSort}
        toggleAsc={this.toggleAsc}
        changeAsc={this.changeAsc}
        // apply sort
        sortData={this.sortData}
        godSearch={this.godSearch}
      />
    );
  }

  renderCardDeck(theInfo) {
    return (
      <section class="my_deck">
        {theInfo &&
          theInfo.map(location_row => (
            <CardDeck>
              {location_row.map(single_location => (
                <Card className="my_card">
                  <CardImg
                    className="card_image"
                    src={single_location.logo_url}
                    alt="Card image caption"
                  />
                  <CardBody>
                    <CardTitle className="card_title">
                      {single_location.city + " " + single_location.year}
                    </CardTitle>
                    <CardText className="card_text">
                      {single_location.overarching_sport}
                    </CardText>
                    <CardText
                      style={
                        this.displayContains(single_location.hosting_country)
                          ? { fontWeight: "bold" }
                          : { fontWeight: "normal" }
                      }
                      className="card_text"
                    >
                      {single_location.hosting_country}{" "}
                    </CardText>
                    <CardText
                      style={
                        this.displayContains(single_location.season)
                          ? { fontWeight: "bold" }
                          : { fontWeight: "normal" }
                      }
                      className="card_text"
                    >
                      {single_location.season.charAt(0).toUpperCase() +
                        single_location.season.slice(1)}
                    </CardText>
                    <CardText className="card_text">
                      {" "}
                      Number of Events: {single_location.num_events}
                    </CardText>
                    <CardText className="card_text">
                      Number of Countries:{" "}
                      {single_location.num_participating_countries}
                    </CardText>
                    <CardText className="card_text">
                      Number of Athletes: {single_location.num_athletes}
                    </CardText>
                    <Button className="card_button">
                      <Link
                        className="card_link"
                        to={"/locations/" + single_location.id}
                      >
                        Learn More
                      </Link>
                    </Button>
                    <br></br>
                    <Button disabled={this.state.compareGroup.length==2 && !this.state.compareGroup.includes(single_location)} onClick={(e) => this.compareChange(single_location)} className="card_button">
															{this.state.compareGroup.includes(single_location) ? "Remove from compare" : "Add to compare"}
													</Button>
                  </CardBody>
                </Card>
              ))}
            </CardDeck>
          ))}
      </section>
    );
  }

  render() {
    let isLoaded = this.state.isLoaded;

    var theInfo = null;

    if (this.state.allLocationsData) {
      theInfo = this.getLocations(
        this.state.page,
        this.state.displayedLocationsData
      );
      console.log("after fetching!!!");
      console.log(theInfo);
    }

    if (!isLoaded) {
      return <div>LOADING...</div>;
    } else {
      return (
        <main>
          <section class="card_deck_header">
            <div class="holder">
              <h1>Locations</h1>
            </div>
          </section>
          <Jumbotron className="locations_jumbotron"></Jumbotron>
          <CompareModal compareGroup={this.state.compareGroup} compareCard={LocationsCompareCard}/>
          {this.renderToolBar()}
          {this.renderCardDeck(theInfo)}
          <AllPagination
            getPage={this.getPage}
            page={this.state.page}
            data={this.state.displayedLocationsData}
          />
        </main>
      );
    }
  }
}

export default AllLocations;
