import React, { Component } from "react";
import { Spinner, Table } from "reactstrap";
import queryString from "query-string";
import { Link } from "react-router-dom";

class Search extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      searchData: null,
      searchQuery: null
    };
  }

  componentDidMount() {
    let url = "https://api.all-olympian.com/api/search?query=";

    const values = queryString.parse(window.location.search);
    if (values != undefined) {
      url += values.query;
      this.setState({ searchQuery: values.query });

      fetch(url)
        .then(res => res.json())
        .then(data => {
          this.setState({ searchData: data, isLoaded: true });
          console.log(this.state.searchData);
        });
    } else {
      // No search query defined
      this.setState({ isLoaded: true });
    }
  }

  highlightTerms = (terms, resultTerm) => {
    console.log("terms: " + terms);
    console.log("lookin for: " + resultTerm);
    for (let i = 0; i < terms.length; i++) {
      let term = terms[i];
      if (resultTerm.toLowerCase().includes(term.toLowerCase())) {
        console.log("found term in terms");
        return <b>{resultTerm}</b>;
      }
    }

    console.log("term not in terms");
    return <div>{resultTerm}</div>;
  };

  render() {
    const { isLoaded, serachData, searchQuery } = this.state;
    if (!isLoaded) {
      return (
        <div>
          SEARCHING....
          <Spinner type="grow" color="primary" />
        </div>
      );
    } else {
      if (this.state.searchData.message != undefined) {
        return <h2>Server error: {this.state.searchData.message}</h2>;
      }

      const countriesResults = this.state.searchData.countries;
      const locationsResults = this.state.searchData.locations;
      const eventsResults = this.state.searchData.events;
      const terms = this.state.searchData.words;

      return (
        <main>
          {countriesResults.length == 0 &&
            locationsResults.length == 0 &&
            eventsResults.length == 0 && <h1>No Results for: {searchQuery}</h1>}

          {countriesResults.length > 0 && <h1>Countries</h1>}

          {countriesResults.length > 0 && (
            <Table bordered>
              <thead className="country_thead">
                <tr>
                  <th>#</th>
                  <th>Countries</th>
                  <th>Capital</th>
                  <th>IOC Code</th>
                  <th>Flag</th>
                  <th>Total Medal Count</th>
                  <th>Population</th>
                </tr>
              </thead>
              <tbody>
                {countriesResults.length > 0 &&
                  countriesResults.map((single_country, i) => (
                    <tr>
                      <th scope="row">{i + 1}</th>
                      <td>
                        <Link to={"/countries/" + single_country.ioc_code}>
                          {" "}
                          {this.highlightTerms(
                            terms,
                            single_country.display_name
                          )}
                        </Link>
                      </td>
                      <td>
                        {this.highlightTerms(terms, single_country.capital)}
                      </td>
                      <td>
                        {this.highlightTerms(terms, single_country.ioc_code)}
                      </td>
                      <td>
                        <Link to={"/countries/" + single_country.ioc_code}>
                          <img
                            src={single_country.country_flag_url}
                            class="allCountriesImage"
                          ></img>
                        </Link>
                      </td>
                      <td>{single_country.medal_count}</td>
                      <td>
                        {single_country.population
                          .toString()
                          .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                      </td>
                    </tr>
                  ))}
              </tbody>
            </Table>
          )}

          {locationsResults.length > 0 && <h1>Locations</h1>}

          {locationsResults.length > 0 && (
            <Table bordered>
              <thead className="country_thead">
                <tr>
                  <th>#</th>
                  <th>City</th>
                  <th>Country</th>
                  <th>Year</th>
                  <th># Athletes</th>
                  <th># Events</th>
                  <th>Season</th>
                  <th>Top Athlete</th>
                  <th>Top Athelete Country</th>
                </tr>
              </thead>
              <tbody>
                {locationsResults.length > 0 &&
                  locationsResults.map((location, i) => (
                    <tr>
                      <th scope="row">{i + 1}</th>
                      <td>
                        <Link to={"/locations/" + location.id}>
                          {" "}
                          {location.city}
                        </Link>
                      </td>
                      <td>
                        {this.highlightTerms(terms, location.hosting_country)}
                      </td>
                      <td>{location.year}</td>
                      <td>{location.num_athletes}</td>
                      <td>{location.num_events}</td>
                      <td>{this.highlightTerms(terms, location.season)}</td>
                      <td>
                        {this.highlightTerms(terms, location.top_athlete)}
                      </td>
                      <td>
                        {this.highlightTerms(
                          terms,
                          location.top_athlete_country
                        )}
                      </td>
                    </tr>
                  ))}
              </tbody>
            </Table>
          )}

          {eventsResults.length > 0 && <h1>Events</h1>}

          {eventsResults.length > 0 && (
            <Table bordered>
              <thead className="country_thead">
                <tr>
                  <th>#</th>
                  <th>Event</th>
                  <th>Sport</th>
                  <th>Gender</th>
                  <th>Latest Champion</th>
                  <th>Latest Champion Country</th>
                  <th>Season</th>
                  <th>Top Athlete</th>
                  <th>Top Athelete Country</th>
                </tr>
              </thead>
              <tbody>
                {eventsResults.length > 0 &&
                  eventsResults.map((event, i) => (
                    <tr>
                      <th scope="row">{i + 1}</th>
                      <td>
                        <Link to={"/events/" + event.id}> {event.event}</Link>
                      </td>
                      <td>
                        {this.highlightTerms(terms, event.overarching_sport)}
                      </td>
                      <td>{this.highlightTerms(terms, event.gender)}</td>
                      <td>
                        {this.highlightTerms(terms, event.latest_champion)}
                      </td>
                      <td>
                        {this.highlightTerms(
                          terms,
                          event.latest_champion_country
                        )}
                      </td>
                      <td>{this.highlightTerms(terms, event.season)}</td>
                      <td>{this.highlightTerms(terms, event.top_athlete)}</td>
                      <td>
                        {this.highlightTerms(terms, event.top_athlete_country)}
                      </td>
                    </tr>
                  ))}
              </tbody>
            </Table>
          )}
        </main>
      );
    }

    return <h1>Search Results</h1>;
  }
}

export default Search;
