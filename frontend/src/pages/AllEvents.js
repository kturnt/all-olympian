import React, { Component } from "react";
import { Link } from "react-router-dom";
import {
  Card,
  Button,
  ButtonToolbar,
  CardImg,
  CardTitle,
  CardText,
  CardDeck,
  CardBody,
  Jumbotron
} from "reactstrap";
import "../styles/CardDeckView.css";
import "../styles/Footer.css";
import AllEventsToolBar from "../components/AllEventsToolBar.js";
import AllPagination from "../components/AllPagination.js";
import CompareModal from "../components/CompareModal";
import EventsCompareCard from "../components/EventsCompareCard";

const numPerPage = 9;
const numPerRow = 3;

class AllEvents extends Component {
  constructor(props) {
    super(props);
    // search
    this.setSearchTerm = this.setSearchTerm.bind(this);
    this.searchEvents = this.searchEvents.bind(this);
    this.displayContains = this.displayContains.bind(this);
    // gender filter
    this.toggleGender = this.toggleGender.bind(this);
    this.changeGender = this.changeGender.bind(this);
    // season filter
    this.toggleSeason = this.toggleSeason.bind(this);
    this.changeSeason = this.changeSeason.bind(this);
    // apply all filters
    this.filterEvents = this.filterEvents.bind(this);
    this.sortData = this.sortData.bind(this);
    this.godSearch = this.godSearch.bind(this);
    // reset all filters
    this.resetFilters = this.resetFilters.bind(this);
    //sort
    this.toggleSort = this.toggleSort.bind(this);
    this.changeSort = this.changeSort.bind(this);
    this.toggleAsc = this.toggleAsc.bind(this);
    this.changeAsc = this.changeAsc.bind(this);
    //compare
    this.compareChange = this.compareChange.bind(this);
    //state
    this.state = {
      page: 1,
      allEventsData: null,
      displayedEventsData: null,
      isLoaded: false,
      genderDropDownOpen: false,
      genderDropDown: "Gender",
      seasonDropDownOpen: false,
      seasonDropDown: "Season",
      sortDropDownOpen: false,
      sortDropDown: "Attribute",
      ascDropDownOpen: false,
      ascDropDown: "Asc.",
      searchTerm: "",
      compareGroup: []
    };
  }

  toggleAsc(event) {
    this.setState({
      ascDropDownOpen: !this.state.ascDropDownOpen
    });
  }

  changeAsc(e) {
    this.setState({ ascDropDown: e.currentTarget.textContent });
    let asc = e.currentTarget.innerText;
    console.log(asc);
  }

  toggleSort(event) {
    this.setState({
      sortDropDownOpen: !this.state.sortDropDownOpen
    });
  }

  changeSort(e) {
    this.setState({ sortDropDown: e.currentTarget.textContent });
    let sortCrit = e.currentTarget.innerText;
    console.log(sortCrit);
  }

  eventCompare(ea, eb) {
    const toComp1 = ea.event.substr(ea.event.indexOf(" ") + 1);
    const toComp2 = eb.event.substr(eb.event.indexOf(" ") + 1);
    return toComp1.localeCompare(toComp2);
  }

  sportCompare(ea, eb) {
    return ea.overarching_sport.localeCompare(eb.overarching_sport);
  }

  genderCompare(ea, eb) {
    return ea.gender.localeCompare(eb.gender);
  }

  seasonCompare(ea, eb) {
    return ea.season.localeCompare(eb.season);
  }

  debutCompare(ea, eb) {
    return ea.debut_year - eb.debut_year;
  }

  sortData() {
    var didChange = false;
    var safeToSort = Array.from(this.state.displayedEventsData);
    if (this.state.sortDropDown === "Event Name") {
      safeToSort = safeToSort.sort(this.eventCompare);
      didChange = true;
    } else if (this.state.sortDropDown === "Sport Name") {
      safeToSort = safeToSort.sort(this.sportCompare);
      didChange = true;
    } else if (this.state.sortDropDown === "Gender") {
      safeToSort = safeToSort.sort(this.genderCompare);
      didChange = true;
    }

    if (this.state.sortDropDown === "Season") {
      safeToSort = safeToSort.sort(this.seasonCompare);
      didChange = true;
    }

    if (this.state.sortDropDown === "Debut Year") {
      safeToSort = safeToSort.sort(this.debutCompare);
      didChange = true;
    }

    // only refresh the page once all the filters (if any) have been applied
    if (didChange) {
      if (this.state.ascDropDown === "Desc.") safeToSort = safeToSort.reverse();
      this.state.displayedEventsData = safeToSort;
      this.state.page = 1;
      this.forceUpdate();
    }
  }

  toggleGender() {
    this.setState({
      genderDropDownOpen: !this.state.genderDropDownOpen
    });
  }

  changeGender(e) {
    this.setState({ genderDropDown: e.currentTarget.textContent });
    let gender = e.currentTarget.innerText;
    console.log(gender);
  }

  toggleSeason() {
    this.setState({
      seasonDropDownOpen: !this.state.seasonDropDownOpen
    });
  }

  changeSeason(e) {
    this.setState({ seasonDropDown: e.currentTarget.textContent });
    let season = e.currentTarget.innerText;
    console.log(season);
  }

  setSearchTerm(val) {
    this.setState({ searchTerm: val });
    console.log(this.state.searchTerm);
  }

  async componentDidMount() {
    const url = "https://api.all-olympian.com//api/events"; //all events
    await fetch(url)
      .then(res => res.json())
      .then(data => {
        this.setState({
          allEventsData: data,
          displayedEventsData: data,
          isLoaded: true
        });
      });
  }

  getPage = pageNum => {
    console.log("inside getPage()");
    this.setState({
      page: pageNum
    });
    return this.getEvents();
  };

  getEvents = (currPage, eventsInfo) => {
    if (!eventsInfo) {
      console.log("ERROR: COUNTRIES INFO NOT FETCHED CORRECTLY");
    }
    // current list of countries to display
    console.log("IN GET EVENTS");
    const correctPage = currPage - 1;
    var startIndex = correctPage * numPerPage;
    let endIndex = startIndex + numPerPage;
    const max = this.state.displayedEventsData.length;
    if (endIndex > max) endIndex = max;
    let outer = [];
    let inner = [];
    while (startIndex < endIndex) {
      //need a new row
      var curEvent = eventsInfo[startIndex];
      inner.push(curEvent);
      if (inner.length % numPerRow == 0) {
        outer.push(inner);
        inner = [];
      }
      startIndex++;
    }
    if (inner.length > 0) {
      outer.push(inner);
    }
    return outer;
  };

  // displayContains
  displayContains = str => {
    if (str === "") return false;
    var splitSearch = this.state.searchTerm.split(/{|,|\.|\s}*/g);
    const toComp = str.toLowerCase();
    for (var i = 0; i < splitSearch.length; i++) {
      var curTerm = splitSearch[i].toLowerCase();
      if (curTerm === "") continue;
      else if (toComp.includes(curTerm)) {
        return true;
      }
    }
    return false;
  };

  filterEvents = () => {
    // temp var for determining if any filtration occurs
    var didChange = false;
    // gender filter
    console.log("IN FILTEREVENTS");
    console.log(this.state.displayedEventsData);
    if (this.state.genderDropDown != "Gender") {
      console.log(this.state.genderDropDown.toLowerCase());
      this.state.displayedEventsData = this.state.displayedEventsData.filter(
        data => data.gender == this.state.genderDropDown.toLowerCase()
      );
      didChange = true;
    }
    // season filter
    if (this.state.seasonDropDown != "Season") {
      console.log(this.state.seasonDropDown);
      this.state.displayedEventsData = this.state.displayedEventsData.filter(
        data => data.season == this.state.seasonDropDown.toLowerCase()
      );
      didChange = true;
    }

    // only refresh the page once all the filters (if any) have been applied
    if (didChange) {
      this.state.page = 1;
      this.forceUpdate();
    }
  };

  // resets state variables to default values, used when clearing filters on data
  resetFilters = () => {
    this.state.displayedEventsData = this.state.allEventsData;
    this.state.page = 1;
    this.state.genderDropDown = "Gender";
    this.state.genderDropDownOpen = false;
    this.state.seasonDropDown = "Season";
    this.state.seasonDropDownOpen = false;
    this.state.sortDropDown = "Attribute";
    this.state.sortDropDownOpen = false;
    this.state.ascDropDown = "Asc.";
    this.state.searchTerm = "";
    var searchBox = document.getElementById("localSearch");
    searchBox.value = "";
    this.state.compareGroup = [];
    this.forceUpdate();
  };

  searchEvents = () => {
    // tokenize our searchTerm on commas, dots, and white space
    // this.state.searchDisabled = true
    if (this.state.searchTerm == "") {
      return;
    }
    var splitSearch = this.state.searchTerm.split(/{|,|\.|\s}*/g);
    console.log("in searchEvents");
    console.log(splitSearch);
    var matchedEvents = [];
    for (var i = 0; i < this.state.displayedEventsData.length; i++) {
      const curEvent = this.state.displayedEventsData[i];
      for (var j = 0; j < splitSearch.length; j++) {
        var curTerm = splitSearch[j].toLowerCase();
        if (curTerm == "") {
          continue;
        } else {
          if (
            curEvent.event.toLowerCase().includes(curTerm) ||
            curEvent.gender.toLowerCase().includes(curTerm) ||
            curEvent.latest_champion.toLowerCase().includes(curTerm) ||
            curEvent.latest_champion_country.toLowerCase().includes(curTerm) ||
            curEvent.overarching_sport.toLowerCase().includes(curTerm) ||
            curEvent.season.toLowerCase().includes(curTerm) ||
            curEvent.top_athlete.toLowerCase().includes(curTerm) ||
            curEvent.top_athlete_country.toLowerCase().includes(curTerm)
          ) {
            if (!matchedEvents.includes(curEvent)) matchedEvents.push(curEvent);
          }
        }
      }
    }
    this.state.displayedEventsData = matchedEvents;
    this.state.page = 1;
    this.forceUpdate();
  };

  godSearch = () => {
    this.state.displayedEventsData = this.state.allEventsData;
    this.filterEvents();
    this.searchEvents();
    if (this.state.sortDropDown != "Attribute") {
      this.sortData();
    }
  };

  compareChange = eventToChange => {
    console.log("CALLED COMPARE CHANGE");
    console.log(this.state.compareGroup);
    if (this.state.compareGroup.includes(eventToChange)) {
      console.log("IN COMPARE GROUP");
      const index = this.state.compareGroup.indexOf(eventToChange);
      this.state.compareGroup.splice(index, 1);
      console.log(this.state.compareGroup);
    } else {
      if (this.state.compareGroup.length >= 2) return;
      this.state.compareGroup.push(eventToChange);
    }
    this.forceUpdate();
  };

  renderToolBar() {
    return (
      <AllEventsToolBar
        // dropdown attributes
        genderDropDownOpen={this.state.genderDropDownOpen}
        genderDropDown={this.state.genderDropDown}
        seasonDropDownOpen={this.state.seasonDropDownOpen}
        seasonDropDown={this.state.seasonDropDown}
        sortDropDownOpen={this.state.sortDropDownOpen}
        sortDropDown={this.state.sortDropDown}
        ascDropDownOpen={this.state.ascDropDownOpen}
        ascDropDown={this.state.ascDropDown}
        // search
        setSearchTerm={this.setSearchTerm}
        searchEvents={this.searchEvents}
        // filter
        toggleGender={this.toggleGender}
        changeGender={this.changeGender}
        toggleSeason={this.toggleSeason}
        changeSeason={this.changeSeason}
        // apply filters
        filterEvents={this.filterEvents}
        // reset filters
        resetFilters={this.resetFilters}
        // sort
        toggleSort={this.toggleSort}
        changeSort={this.changeSort}
        toggleAsc={this.toggleAsc}
        changeAsc={this.changeAsc}
        // apply sort
        sortData={this.sortData}
        godSearch={this.godSearch}
      />
    );
  }

  renderCardDeck(theInfo) {
    return (
      <section class="my_deck">
        {theInfo &&
          theInfo.map(event_row => (
            <CardDeck>
              {event_row.map(single_event => (
                <Card className="my_card">
                  <CardImg
                    className="card_image"
                    src={single_event.image_url}
                    alt="Card image caption"
                  />
                  <CardBody>
                    <CardTitle
                      style={
                        this.displayContains(single_event.event)
                          ? { fontWeight: "bold" }
                          : { fontWeight: "normal" }
                      }
                      className="card_title"
                    >
                      {single_event.event}
                    </CardTitle>
                    <CardText
                      style={
                        this.displayContains(single_event.overarching_sport)
                          ? { fontWeight: "bold" }
                          : { fontWeight: "normal" }
                      }
                      className="card_text"
                    >
                      {single_event.overarching_sport}
                    </CardText>
                    <CardText
                      style={
                        this.displayContains(single_event.gender)
                          ? { fontWeight: "bold" }
                          : { fontWeight: "normal" }
                      }
                      className="card_text"
                    >
                      {single_event.gender.charAt(0).toUpperCase() +
                        single_event.gender.slice(1)}{" "}
                    </CardText>
                    <CardText
                      style={
                        this.displayContains(single_event.season)
                          ? { fontWeight: "bold" }
                          : { fontWeight: "normal" }
                      }
                      className="card_text"
                    >
                      {single_event.season.charAt(0).toUpperCase() +
                        single_event.season.slice(1)}
                    </CardText>
                    {single_event.debut_year != null ? (
                      <CardText className="card_text">
                        Debut Year: {single_event.debut_year.toString()}
                      </CardText>
                    ) : (
                      ""
                    )}
                    <Button>
                      <Link
                        className="card_link"
                        to={"/events/" + single_event.id}
                      >
                        Learn More
                      </Link>
                    </Button>
                    <Button
					  disabled={this.state.compareGroup.length==2 && !this.state.compareGroup.includes(single_event)}
                      onClick={e => this.compareChange(single_event)}
                      className="card_button"
                    >
                      {this.state.compareGroup.includes(single_event)
                        ? "Remove from compare"
                        : "Add to compare"}
                    </Button>
                  </CardBody>
                </Card>
              ))}
            </CardDeck>
          ))}
      </section>
    );
  }

  render() {
    let isLoaded = this.state.isLoaded;

    let theInfo = null;
    if (this.state.allEventsData) {
      theInfo = this.getEvents(this.state.page, this.state.displayedEventsData);
      console.log(theInfo);
    }
    if (!isLoaded) {
      return <div>LOADING...</div>;
    } else {
      return (
        <main>
          <section class="card_deck_header">
            <div class="holder">
              <h1>Events</h1>
            </div>
          </section>
          <Jumbotron className="events_jumbotron"></Jumbotron>
          <CompareModal
            compareCard={EventsCompareCard}
            compareGroup={this.state.compareGroup}
          />
          {this.renderToolBar()}
          {this.renderCardDeck(theInfo)}
          <AllPagination
            getPage={this.getPage}
            page={this.state.page}
            data={this.state.displayedEventsData}
          />
        </main>
      );
    }
  }
}
export default AllEvents;
