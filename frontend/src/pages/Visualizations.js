import React, { Component, useState } from "react";
import { Container, Row, Col } from "reactstrap";
import {
  VictoryBar,
  VictoryChart,
  VictoryPie,
  VictoryLine,
  VictoryScatter,
  VictoryTheme,
  VictoryAxis,
  VictoryLegend
} from "victory";
import "../styles/Visualizations.css";

class Visualizations extends Component {
  constructor(props) {
    super(props);
    this.state = {
      summerGraph: null,
      winterGraph: null,
      countriesScatterPlot: null,
      summerLocationsGraph: null,
      winterLocationsGraph: null,
      statesGraph: null,
      trailsGraph: null
    };
  }

  trailComp(ta, tb) {
    return ta.y - tb.y;
  }

  async componentDidMount() {
    const eventsUrl = "https://api.all-olympian.com/api/events"; //all events
    fetch(eventsUrl)
      .then(res => res.json())
      .then(data => {
        var summerGraph = [
          { x: "Men", y: 0 },
          { x: "Women", y: 0 },
          { x: "Mixed", y: 0 }
        ];
        var winterGraph = [
          { x: "Men", y: 0 },
          { x: "Women", y: 0 },
          { x: "Mixed", y: 0 }
        ];
        for (var event of data) {
          console.log(event);
          if (event["season"] == "summer") {
            if (event["gender"] == "male") {
              summerGraph[0].y++;
            } else if (event["gender"] == "female") {
              summerGraph[1].y++;
            } else {
              summerGraph[2].y++;
            }
          } else if (event["season"] == "winter") {
            if (event["gender"] == "male") {
              winterGraph[0].y++;
            } else if (event["gender"] == "female") {
              winterGraph[1].y++;
            } else {
              winterGraph[2].y++;
            }
          }
        }
        this.setState({
          summerGraph: summerGraph,
          winterGraph: winterGraph
        });
      });
    const countriesUrl = "https://api.all-olympian.com/api/countries";
    fetch(countriesUrl)
      .then(res => res.json())
      .then(data => {
        var countriesGraph = [];
        for (var country of data) {
          console.log(country);
          countriesGraph.push({
            x:
              parseFloat(country["gdp"].replace("$", "").replace(/,/g, "")) /
              1000000000,
            y: country["medal_count"] / 1000
          });
        }
        this.setState({
          countriesGraph: countriesGraph
        });
      });

    const url = "https://api.all-olympian.com/api/olympics"; //all locations
    fetch(url)
      .then(res => res.json())
      .then(data => {
        var summerLocationsGraph = [];
        var winterLocationsGraph = [];
        for (var location of data) {
          if (location.season == "summer") {
            summerLocationsGraph.push({
              x: location.year,
              y: location.num_participating_countries
            });
          } else {
            winterLocationsGraph.push({
              x: location.year,
              y: location.num_participating_countries
            });
          }
        }
        this.setState({
          summerLocationsGraph: summerLocationsGraph,
          winterLocationsGraph: winterLocationsGraph
        });
      });

    const statesUrl = "https://api.hikeadvisor.me/api/state?page=";
    const statesData = [{ x: "0-1000ft", y: 0 }];
    statesData.push({ x: "1000-2000ft", y: 0 });
    statesData.push({ x: "2000-3000ft", y: 0 });
    statesData.push({ x: "3000-4000ft", y: 0 });
    statesData.push({ x: "4000-5000ft", y: 0 });
    statesData.push({ x: "5000-6000ft", y: 0 });
    statesData.push({ x: "6000-7000ft", y: 0 });
    for (var i = 1; i <= 5; i++) {
      await fetch(statesUrl + i.toString())
        .then(res => res.json())
        .then(data => {
          for (var state of data.objects) {
            if (state.state_elevation <= 1000.0) {
              statesData[0].y++;
            } else if (state.state_elevation <= 2000.0) {
              statesData[1].y++;
            } else if (state.state_elevation <= 3000.0) {
              statesData[2].y++;
            } else if (state.state_elevation <= 4000.0) {
              statesData[3].y++;
            } else if (state.state_elevation <= 5000.0) {
              statesData[4].y++;
            } else if (state.state_elevation <= 6000.0) {
              statesData[5].y++;
            } else {
              statesData[6].y++;
            }
          }
        });
    }
    this.setState({ statesGraph: statesData });

    // animals by taxonomy
    const animalUrl = "https://api.hikeadvisor.me/api/animal?page=";
    const animalData = {};
    for (var i = 1; i <= 15; i++) {
      await fetch(animalUrl + i.toString())
        .then(res => res.json())
        .then(data => {
          for (var animal of data.objects) {
            if (
              animal.animal_taxonName != "Animalia" &&
              animal.animal_taxonName != "Plantae" &&
              animal.animal_taxonName != "Fungi"
            ) {
              if (animalData[animal.animal_taxonName] == undefined) {
                animalData[animal.animal_taxonName] = 0;
              } else {
                animalData[animal.animal_taxonName] += animal.animal_numObser;
              }
            }
          }
          console.log("out of inner for");
        });
    }
    console.log("out of outer for:");
    console.log(animalData);
    const animalArr = [];
    for (var key in animalData) {
      animalArr.push({ x: key, y: animalData[key] });
    }
    this.setState({ animalGraph: animalArr });

    const trailUrl = "https://api.hikeadvisor.me/api/trail?page=";
    var trailsData = {};
    for (var i = 1; i <= 53; i++) {
      await fetch(trailUrl + i.toString())
        .then(res => res.json())
        .then(data => {
          for (var trail of data.objects) {
            if (trailsData[trail.trail_states] == undefined) {
              var stars = [];
              stars.push(trail.trail_stars);
              trailsData[trail.trail_states] = stars;
            }
            trailsData[trail.trail_states].push(trail.trail_stars);
          }
          console.log("out of inner for");
        });
    }
    console.log("out of outer for:");
    for (var key in trailsData) {
      var curTotal = 0;
      for (var i = 0; i < trailsData[key].length; i++) {
        curTotal += trailsData[key][i];
      }
      var avg = curTotal / trailsData[key].length;
      trailsData[key] = avg;
    }
    var trailsArr = [];
    for (var key in trailsData) {
      trailsArr.push({ x: key, y: trailsData[key] });
    }

    trailsArr.sort(this.trailComp);
    var dataArr = trailsArr.slice(0, 10);
    console.log("trails data");
    console.log(trailsArr);
    this.setState({ trailsGraph: dataArr });
  }

  render() {
    return (
      <main>
        <section class="visualizations_header">
          <div class="holder">
            <h1>All Olympian Visualizations</h1>
          </div>
        </section>
        <Container className="graph-container">
          <Row>
            <Col className="events-graph-col">
              <h1>Summer Events by Gender</h1>
              <VictoryPie
                className="pie-graph"
                data={this.state.summerGraph}
                colorScale={["tomato", "orange", "gold"]}
                width="800"
              />
            </Col>
            <Col className="events-graph-col">
              <h1>Winter Events by Gender</h1>
              <VictoryPie
                className="pie-graph"
                data={this.state.winterGraph}
                colorScale={["blue", "cyan", "navy"]}
                width="800"
              />
            </Col>
          </Row>
        </Container>

        <Container className="graph-container">
          <h1>Country GDP vs. Medal Count</h1>
          <VictoryChart
            className="scatter-plot"
            theme={VictoryTheme.material}
            width="900"
          >
            <VictoryScatter
              style={{ data: { stroke: "red" } }}
              className="scatterPlot"
              data={this.state.countriesGraph}
              colorScale={["tomato", "orange", "gold", "cyan", "navy"]}
              width="900"
            />
            <VictoryAxis
              dependentAxis={false}
              orientation="bottom"
              fixLabelOverlap={true}
              label={"GDP (in billions)"}
              style={{ axisLabel: { fontSize: 10, padding: 30 } }}
            ></VictoryAxis>
            <VictoryAxis
              dependentAxis={true}
              orientation="left"
              fixLabelOverlap={true}
              label={"Medal Count (in thousands)"}
              style={{ axisLabel: { fontSize: 10, padding: 30 } }}
            ></VictoryAxis>
          </VictoryChart>
        </Container>

        <Container className="graph-container">
          <h1>Summer and Winter Olympics Events over Time</h1>
          <VictoryChart
            className="line-graph"
            theme={VictoryTheme.material}
            width="900"
          >
            <VictoryLine
              style={{ data: { stroke: "red" } }}
              data={this.state.summerLocationsGraph}
            />
            <VictoryLine
              style={{ data: { stroke: "blue" } }}
              data={this.state.winterLocationsGraph}
            />
            <VictoryAxis
              dependentAxis={false}
              orientation="bottom"
              fixLabelOverlap={true}
              label={"Year"}
              style={{ axisLabel: { fontSize: 10, padding: 30 } }}
            ></VictoryAxis>
            <VictoryAxis
              dependentAxis={true}
              orientation="left"
              fixLabelOverlap={true}
              label={"Number of Events"}
              style={{ axisLabel: { fontSize: 10, padding: 30 } }}
            ></VictoryAxis>
            <VictoryLegend
              x={125}
              y={50}
              centerTitle
              orientation="horizontal"
              gutter={20}
              style={{ border: { stroke: "black" }, title: { fontSize: 20 } }}
              data={[
                { name: "Summer", symbol: { fill: "red" } },
                { name: "Winter", symbol: { fill: "blue" } }
              ]}
            />
          </VictoryChart>
        </Container>

        <section class="visualizations_header">
          <div class="holder">
            <h1>HikeAdvisor Visualizations</h1>
          </div>
        </section>
        <Container className="graph-container">
          <Row>
            <Col className="events-graph-col">
              <h1>Animal Sightings by Taxonomy</h1>
              <VictoryPie
                className="pie-graph"
                data={this.state.animalGraph}
                colorScale={[
                  "red",
                  "orange",
                  "yellow",
                  "green",
                  "blue",
                  "violet"
                ]}
                width="800"
              />
            </Col>
          </Row>
        </Container>

        <Container className="graph-container">
          <Row>
            <Col className="events-graph-col">
              <h1>Number of States per Elevation Range</h1>
              <VictoryChart
                theme={VictoryTheme.material}
                domainPadding={{ x: 25 }}
              >
                <VictoryBar
                  className="bar-graph"
                  data={this.state.statesGraph}
                  colorScale={[
                    "red",
                    "orange",
                    "yellow",
                    "green",
                    "blue",
                    "violet"
                  ]}
                  width="800"
                />
                <VictoryAxis
                  dependentAxis={false}
                  orientation="bottom"
                  fixLabelOverlap={true}
                  label={"Elevation"}
                  style={{ axisLabel: { fontSize: 10, padding: 30 } }}
                ></VictoryAxis>
                <VictoryAxis
                  dependentAxis={true}
                  orientation="left"
                  fixLabelOverlap={true}
                  label={"Number of States"}
                  style={{ axisLabel: { fontSize: 10, padding: 30 } }}
                ></VictoryAxis>
              </VictoryChart>
            </Col>
          </Row>
        </Container>

        <Container className="graph-container">
          <Row>
            <Col className="events-graph-col">
              <h1>Top 10 States by Average Trail Rating</h1>
              <VictoryChart theme={VictoryTheme.material}>
                <VictoryBar
                  className="bar-graph"
                  data={this.state.trailsGraph}
                />
                <VictoryAxis
                  dependentAxis={false}
                  orientation="bottom"
                  label={"States"}
                  style={{
                    axisLabel: { fontSize: 10, padding: 30 },
                    tickLabels: { fontSize: 3 }
                  }}
                ></VictoryAxis>
                <VictoryAxis
                  dependentAxis={true}
                  orientation="left"
                  label={"Average Trail Rating"}
                  style={{ axisLabel: { fontSize: 10, padding: 30 } }}
                ></VictoryAxis>
              </VictoryChart>
            </Col>
          </Row>
        </Container>
      </main>
    );
  }
}

export default Visualizations;
