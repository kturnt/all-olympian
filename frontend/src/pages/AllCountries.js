import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Table, Jumbotron, Button } from "reactstrap";
import AllPagination from "../components/AllPagination.js";
import AllCountriesToolBar from "../components/AllCountriesToolBar.js";
import "../styles/TableView.css";
import "../styles/Footer.css";
import CompareModal from '../components/CompareModal';
import CountriesCompareCard from '../components/CountriesCompareCard';

const numPerPage = 10;

class AllCountries extends Component {
  constructor(props) {
    super(props);
    // search
    this.setSearchTerm = this.setSearchTerm.bind(this);
    this.searchCountries = this.searchCountries.bind(this);
    this.displayContains = this.displayContains.bind(this);
    // filter region
    this.toggleRegion = this.toggleRegion.bind(this);
    this.changeRegion = this.changeRegion.bind(this);
    // filter population
    this.togglePopulation = this.togglePopulation.bind(this);
    this.setMinPop = this.setMinPop.bind(this);
    this.setMaxPop = this.setMaxPop.bind(this);
    this.changePopulation = this.changePopulation.bind(this);
    // filter gdp
    this.toggleGDP = this.toggleGDP.bind(this);
    this.setMinGDP = this.setMinGDP.bind(this);
    this.setMaxGDP = this.setMaxGDP.bind(this);
    this.changeGDP = this.changeGDP.bind(this);
    // filter medal count
    this.toggleMedal = this.toggleMedal.bind(this);
    this.setMinMedal = this.setMinMedal.bind(this);
    this.setMaxMedal = this.setMaxMedal.bind(this);
    this.changeMedal = this.changeMedal.bind(this);
    // apply filters
    this.filterCountries = this.filterCountries.bind(this);
    this.sortData = this.sortData.bind(this);
    this.godSearch = this.godSearch.bind(this);
    //sort
    this.toggleSort = this.toggleSort.bind(this);
    this.changeSort = this.changeSort.bind(this);
    this.toggleAsc = this.toggleAsc.bind(this);
    this.changeAsc = this.changeAsc.bind(this);
    //compare
    this.compareChange = this.compareChange.bind(this);
    //state
    this.state = {
      page: 1,
      allCountriesData: null,
      displayedCountriesData: null,
      isLoaded: false,
      regionDropDownOpen: false,
      regionDropDown: "Region",
      populationDropDownOpen: false,
      populationDropDown: "Population",
      populationMin: 0,
      populationMax: Number.MAX_SAFE_INTEGER,
      GDPDropDownOpen: false,
      GDPDropDown: "GDP",
      sortDropDownOpen: false,
      sortDropDown: "Attribute",
      ascDropDownOpen: false,
      ascDropDown: "Asc.",
      GDPMin: 0,
      GDPMax: Number.MAX_SAFE_INTEGER,
      medalDropDown: "Medal Count",
      medalDropDownOpen: false,
      medalMin: 0,
      medalMax: Number.MAX_SAFE_INTEGER,
      searchTerm: "",
      compareGroup: []
    };
  }

  toggleAsc(event) {
    this.setState({
      ascDropDownOpen: !this.state.ascDropDownOpen
    });
  }

  changeAsc(e) {
    this.setState({ ascDropDown: e.currentTarget.textContent });
    let asc = e.currentTarget.innerText;
    console.log(asc);
  }

  toggleSort(event) {
    this.setState({
      sortDropDownOpen: !this.state.sortDropDownOpen
    });
  }

  changeSort(e) {
    this.setState({ sortDropDown: e.currentTarget.textContent });
    let sortCrit = e.currentTarget.innerText;
    console.log(sortCrit);
  }

  regionCompare(ra, rb) {
    return ra.region.localeCompare(rb.region);
  }

  countryCompare(ca, cb) {
    return ca.name.localeCompare(cb.name);
  }

  iocCompare(ca, cb) {
    return ca.ioc_code.localeCompare(cb.ioc_code);
  }

  medalCompare(ca, cb) {
    return ca.medal_count - cb.medal_count;
  }

  popCompare(ca, cb) {
    return ca.population - cb.population;
  }

  gdpCompare(ca, cb) {
    return (
      parseFloat(ca.gdp.replace("$", "").replace(/,/g, "")) -
      parseFloat(cb.gdp.replace("$", "").replace(/,/g, ""))
    );
  }

  sortData() {
    var didChange = false;
    var safeToSort = Array.from(this.state.displayedCountriesData);
    if (this.state.sortDropDown === "Region") {
      safeToSort = safeToSort.sort(this.regionCompare);
      didChange = true;
    } else if (this.state.sortDropDown === "Country Name") {
      safeToSort = safeToSort.sort(this.countryCompare);
      didChange = true;
    } else if (this.state.sortDropDown === "IOC Code") {
      safeToSort = safeToSort.sort(this.iocCompare);
      didChange = true;
    }

    if (this.state.sortDropDown === "Medal Count") {
      safeToSort = safeToSort.sort(this.medalCompare);
      didChange = true;
    }

    if (this.state.sortDropDown === "Population") {
      safeToSort = safeToSort.sort(this.popCompare);
      didChange = true;
    }

    if (this.state.sortDropDown === "GDP") {
      safeToSort = safeToSort.sort(this.gdpCompare);
      didChange = true;
    }

    // only refresh the page once all the filters (if any) have been applied
    if (didChange) {
      if (this.state.ascDropDown === "Desc.") safeToSort = safeToSort.reverse();
      this.state.displayedCountriesData = safeToSort;
      this.state.page = 1;
      this.forceUpdate();
    }
  }

  toggleRegion(event) {
    this.setState({
      regionDropDownOpen: !this.state.regionDropDownOpen
    });
  }

  changeRegion(e) {
    this.setState({ regionDropDown: e.currentTarget.textContent });
    let region = e.currentTarget.innerText;
    console.log(region);
  }

  togglePopulation(event) {
    this.setState({
      populationDropDownOpen: !this.state.populationDropDownOpen
    });
  }

  changePopulation() {
    var population = "";
    if (this.state.populationMax === Number.MAX_SAFE_INTEGER) {
      population = "Population: " + ">= " + this.state.populationMin;
    } else {
      population =
        "Population: " +
        this.state.populationMin +
        "-" +
        this.state.populationMax;
    }
    this.setState({ populationDropDown: population });
    console.log(population);
  }

  toggleGDP() {
    this.setState({ GDPDropDownOpen: !this.state.GDPDropDownOpen });
  }

  changeGDP() {
    var gdp = "";
    if (this.state.GDPMax === Number.MAX_SAFE_INTEGER) {
      gdp = "GDP: " + ">= " + this.state.GDPMin;
    } else {
      gdp = "GDP: " + this.state.GDPMin + "-" + this.state.GDPMax;
    }
    this.setState({ GDPDropDown: gdp });
  }

  toggleMedal(event) {
    this.setState({
      medalDropDownOpen: !this.state.medalDropDownOpen
    });
  }

  changeMedal() {
    var medal = "";
    if (this.state.medalMax === Number.MAX_SAFE_INTEGER) {
      medal = "Medal Count: " + ">= " + this.state.medalMin;
    } else {
      medal = "Medal Count: " + this.state.medalMin + "-" + this.state.medalMax;
    }
    this.setState({ medalDropDown: medal });
    console.log(medal);
  }

  setMinGDP(val) {
    this.setState({ GDPMin: val });
    console.log(this.state.GDPMin);
  }

  setMaxGDP(val) {
    this.setState({ GDPMax: val });
    console.log(this.state.GDPMax);
  }
  setMinPop(val) {
    this.setState({ populationMin: val });
    console.log(this.state.populationMin);
  }

  setMaxPop(val) {
    this.setState({ populationMax: val });
    console.log(this.state.populationMax);
  }

  setSearchTerm(val) {
    this.setState({ searchTerm: val });
    console.log(this.state.searchTerm);
  }

  setMinMedal(val) {
    this.setState({ medalMin: val });
    console.log(this.state.medalMin);
  }

  setMaxMedal(val) {
    this.setState({ medalMax: val });
    console.log(this.state.medalMax);
  }

  async componentDidMount() {
    const url = "https://api.all-olympian.com/api/countries/"; //all countries
    await fetch(url)
      .then(res => res.json())
      .then(data => {
        this.setState({
          allCountriesData: data.sort(this.countryCompare),
          displayedCountriesData: data.sort(this.countryCompare),
          isLoaded: true
        });
      });
  }

  getPage = pageNum => {
    console.log("inside getPage()");
    this.setState({
      page: pageNum
    });
    return this.getCountries();
  };

  getCountries = (currPage, countriesInfo) => {
    if (!countriesInfo) {
      console.log("ERROR: COUNTRIES INFO NOT FETCHED CORRECTLY");
    }
    // current list of countries to display
    const correctPage = currPage - 1;
    var startIndex = correctPage * numPerPage;
    let endIndex = startIndex + numPerPage;
    const max = this.state.displayedCountriesData.length;
    if (endIndex > max) endIndex = max;
    let currentCountries = [];
    while (startIndex < endIndex) {
      var curCountry = countriesInfo[startIndex];
      currentCountries.push(curCountry);
      startIndex++;
    }

    return currentCountries;
  };

  // filters the current data set to work from by modifying displayedCountriesData var in the state
  // NOTE: we retain allCountriesData in the state in order to maintain the original context for clearAll
  filterCountries = () => {
    // temp var for determining if any filtration occurs
    var didChange = false;
    // this.state.displayedCountriesData = this.state.allCountriesData
    if (this.state.regionDropDown != "Region") {
      this.state.displayedCountriesData = this.state.displayedCountriesData.filter(
        data => data.region == this.state.regionDropDown
      );
      didChange = true;
    }
    if (this.state.populationDropDown != "Population") {
      this.state.displayedCountriesData = this.state.displayedCountriesData.filter(
        data =>
          data.population >= this.state.populationMin &&
          data.population <= this.state.populationMax
      );
      didChange = true;
    }
    if (this.state.GDPDropDown != "GDP") {
      console.log("GDP: ");

      console.log(
        parseFloat(
          this.state.displayedCountriesData[0].gdp
            .replace("$", "")
            .replace(/,/g, "")
        )
      );
      this.state.displayedCountriesData = this.state.displayedCountriesData.filter(
        data =>
          parseFloat(data.gdp.replace("$", "").replace(/,/g, "")) >=
            this.state.GDPMin &&
          parseFloat(data.gdp.replace("$", "").replace(/,/g, "")) <=
            this.state.GDPMax
      );
      didChange = true;
    }
    if (this.state.medalDropDown != "Medal Count") {
      this.state.displayedCountriesData = this.state.displayedCountriesData.filter(
        data =>
          data.medal_count >= this.state.medalMin &&
          data.medal_count <= this.state.medalMax
      );
      didChange = true;
    }

    // only refresh the page once all the filters (if any) have been applied
    if (didChange) {
      this.state.page = 1;
      this.forceUpdate();
    }
  };

  // displayContains
  displayContains = str => {
    if (str === "") return false;
    var splitSearch = this.state.searchTerm.split(/{|,|\.|\s}*/g);
    const toComp = str.toLowerCase();
    for (var i = 0; i < splitSearch.length; i++) {
      var curTerm = splitSearch[i].toLowerCase();
      if (curTerm === "") continue;
      else if (toComp.includes(curTerm)) {
        return true;
      }
    }
    return false;
  };

  // search the current displayed countries by searchTerm, iterates over the dataset and compares to a processed searchTerm
  searchCountries = () => {
    // tokenize our searchTerm on commas, dots, and white space

    if (this.state.searchTerm == "") {
      return;
    }
    var splitSearch = this.state.searchTerm.split(/{|,|\.|\s}*/g);
    console.log("in searchCountries");
    console.log(splitSearch);
    var matchedCountries = [];
    for (var i = 0; i < this.state.displayedCountriesData.length; i++) {
      const curCountry = this.state.displayedCountriesData[i];
      for (var j = 0; j < splitSearch.length; j++) {
        var curTerm = splitSearch[j].toLowerCase();
        if (curTerm == "") {
          continue;
        } else {
          if (
            curCountry.capital.toLowerCase().includes(curTerm) ||
            curCountry.display_name.toLowerCase().includes(curTerm) ||
            curCountry.region.toLowerCase().includes(curTerm) ||
            curCountry.ioc_code.toLowerCase().includes(curTerm)
          ) {
            if (!matchedCountries.includes(curCountry))
              matchedCountries.push(curCountry);
          }
          // iterate languages
          for (var k = 0; k < curCountry.languages.length; k++) {
            var curLang = curCountry.languages[k];
            if (curLang.toLowerCase().includes(curTerm)) {
              if (!matchedCountries.includes(curCountry))
                matchedCountries.push(curCountry);
            }
          }
        }
      }
    }
    this.state.displayedCountriesData = matchedCountries;
    this.state.page = 1;
    this.forceUpdate();
  };

  godSearch = () => {
    this.state.displayedCountriesData = this.state.allCountriesData;
    this.filterCountries();
    this.searchCountries();
    if (this.state.sortDropDown != "Attribute") {
      this.sortData();
    }
  };

  // resets state variables to default values, used when clearing filters on data
  resetFilters = () => {
    this.state.displayedCountriesData = this.state.allCountriesData;
    this.state.page = 1;
    this.state.regionDropDown = "Region";
    this.state.populationDropDown = "Population";
    this.state.GDPDropDown = "GDP";
    this.state.medalDropDown = "Medal Count";
    this.state.sortDropDown = "Attribute";
    this.state.populationMin = 0;
    this.state.populationMax = Number.MAX_SAFE_INTEGER;
    this.state.GDPMin = 0;
    this.state.GDPMax = Number.MAX_SAFE_INTEGER;
    this.state.medalMin = 0;
    this.state.medalMax = Number.MAX_SAFE_INTEGER;
    this.state.ascDropDown = "Asc.";
    this.state.searchTerm = "";
    var searchBox = document.getElementById("localSearch");
    searchBox.value = "";
    this.state.compareGroup = [];
    this.forceUpdate();
  };

  compareChange = (countryToChange) => {
		console.log("CALLED COMPARE CHANGE")
		console.log(this.state.compareGroup)
		if(this.state.compareGroup.includes(countryToChange)){
			console.log("IN COMPARE GROUP")
			const index = this.state.compareGroup.indexOf(countryToChange);
			this.state.compareGroup.splice(index, 1);
			console.log(this.state.compareGroup)
		}
		else{
			if(this.state.compareGroup.length >= 2)
				return;
			this.state.compareGroup.push(countryToChange)
		}
		this.forceUpdate()
  }

  renderToolBar() {
    return (
      <AllCountriesToolBar
        // dropdown attributes
        regionDropDownOpen={this.state.regionDropDownOpen}
        regionDropDown={this.state.regionDropDown}
        populationDropDownOpen={this.state.populationDropDownOpen}
        populationDropDown={this.state.populationDropDown}
        GDPDropDownOpen={this.state.GDPDropDownOpen}
        GDPDropDown={this.state.GDPDropDown}
        sortDropDownOpen={this.state.sortDropDownOpen}
        sortDropDown={this.state.sortDropDown}
        ascDropDownOpen={this.state.ascDropDownOpen}
        ascDropDown={this.state.ascDropDown}
        medalDropDown={this.state.medalDropDown}
        medalDropDownOpen={this.state.medalDropDownOpen}
        // search
        setSearchTerm={this.setSearchTerm}
        searchCountries={this.searchCountries}
        // filter
        toggleRegion={this.toggleRegion}
        changeRegion={this.changeRegion}
        togglePopulation={this.togglePopulation}
        setMinPop={this.setMinPop}
        setMaxPop={this.setMaxPop}
        changePopulation={this.changePopulation}
        toggleGDP={this.toggleGDP}
        setMinGDP={this.setMinGDP}
        setMaxGDP={this.setMaxGDP}
        changeGDP={this.changeGDP}
        toggleMedal={this.toggleMedal}
        setMinMedal={this.setMinMedal}
        setMaxMedal={this.setMaxMedal}
        changeMedal={this.changeMedal}
        // apply filters
        filterCountries={this.filterCountries}
        // reset filters
        resetFilters={this.resetFilters}
        // sort
        toggleSort={this.toggleSort}
        changeSort={this.changeSort}
        toggleAsc={this.toggleAsc}
        changeAsc={this.changeAsc}
        // apply sort
        sortData={this.sortData}
        // god search
        godSearch={this.godSearch}
      />
    );
  }

  renderTable(theInfo) {
    return (
      <section className="country_table">
        <Table bordered>
          <thead className="country_thead">
            <tr>
              <th>#</th>
              <th>Countries</th>
              <th>IOC Code</th>
              <th>Flag</th>
              <th>Total Medal Count</th>
              <th>Population</th>
              <th>GDP</th>
              <th>Compare</th>
            </tr>
          </thead>
          <tbody>
            {// conditional rendering, considering we need for the async callback
            theInfo &&
              theInfo.map((single_country, i) => (
                <tr>
                  <th scope="row">{i + (this.state.page - 1) * 10 + 1}</th>
                  <td
                    style={
                      this.displayContains(single_country.display_name)
                        ? { fontWeight: "bold" }
                        : { fontWeight: "normal" }
                    }
                  >
                    <Link to={"/countries/" + single_country.ioc_code}>
                      {" "}
                      {single_country.display_name}
                    </Link>
                  </td>
                  <td
                    style={
                      this.displayContains(single_country.ioc_code)
                        ? { fontWeight: "bold" }
                        : { fontWeight: "normal" }
                    }
                  >
                    {single_country.ioc_code}
                  </td>
                  <td>
                    <Link to={"/countries/" + single_country.ioc_code}>
                      <img
                        src={single_country.country_flag_url}
                        class="allCountriesImage"
                      ></img>
                    </Link>
                  </td>
                  <td>{single_country.medal_count}</td>
                  <td>
                    {single_country.population
                      .toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                  </td>
                  <td>
                    {single_country.gdp
                      .toString()
                      .replace(/\B(?=(\d{3})+(?!\d))/g, ",")}
                  </td>
                  <td>
                      <Button disabled={this.state.compareGroup.length==2 && !this.state.compareGroup.includes(single_country)} onClick={(e) => this.compareChange(single_country)}>
													{this.state.compareGroup.includes(single_country) ? "Remove from compare" : "Add to compare"}
											</Button>
                  </td>
                </tr>
              ))}
          </tbody>
        </Table>
      </section>
    );
  }

  render() {
    let isLoaded = this.state.isLoaded;

    let theInfo = null;
    console.log(this.state.allCountriesData);
    if (this.state.allCountriesData) {
      theInfo = this.getCountries(
        this.state.page,
        this.state.displayedCountriesData
      );
      // console.log(allCountries)
    }

    if (!isLoaded) {
      return <div>LOADING...</div>;
    } else {
      return (
        <main>
          <section class="country_table_header">
            <div class="holder">
              <h1 class="country_table_title">Countries</h1>
            </div>
          </section>
          <Jumbotron className="countries_jumbotron"></Jumbotron>
          <CompareModal compareCard={CountriesCompareCard} compareGroup={this.state.compareGroup}/>
		  {this.renderToolBar()}
          {this.renderTable(theInfo)}
          <AllPagination
            getPage={this.getPage}
            page={this.state.page}
            data={this.state.displayedCountriesData}
          />
        </main>
      );
    }
  }
}
export default AllCountries;
