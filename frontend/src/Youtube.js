const KEY = "AIzaSyB5KvbdJdWfbob5f6bkg5Uo0BOqO4mS5l0"
const baseUrl = "https://www.googleapis.com/youtube/v3/search"
const YoutubeURL = baseUrl + "?part=snippet&maxResults=1&type=video&videoEmbeddable=true&key=" + KEY
const YoutubeOpts = {
	height: "390",
	width: "640",
	playerVars: {
		autoplay: 0
	}
}

export { YoutubeURL, YoutubeOpts }
