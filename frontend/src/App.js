import React from 'react';
import { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import Header from './components/Header';
import MainRouter from './routers/MainRouter';


function App(props) {
  return (
    <div className="App">
      <Header/>
      <MainRouter/>
    </div>
  );
}

export default App;
