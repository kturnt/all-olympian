from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd

def get_locations():
    urls = []
    driver = webdriver.Chrome('/usr/local/bin/chromedriver')

    driver.get("https://www.olympic.org/olympic-games")
    content = driver.page_source
    soup = BeautifulSoup(content, "html.parser")
    
    game_boxes = soup.findAll("ul", class_="game-boxes")
    #print(len(game_boxes))
    for i in range(0, len(game_boxes) - 1):
        uls = game_boxes[i].findAll("a", href=True)
        newurls = [ul["href"] for ul in uls]
        urls += newurls

    driver.close()

    return urls[5:]

if __name__ == "__main__":
    get_locations()
