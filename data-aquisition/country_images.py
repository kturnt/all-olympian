import json
import image_search

if __name__ == "__main__" :
    with open("countries.json", "r") as countries :
        data = json.load(countries)
        entry_list = []
        for country in data :
            query = country["display name"] + " " + country["capital"] + " skyline"
            capital_image = image_search.search(query, 1)[0]
            country["capital image"] = capital_image

            query = "olympics team " + country["display name"]
            olympic_image = image_search.search(query, 1)[0]
            country["olympics image"] = olympic_image
            entry_list.append(country)
        print(json.dumps(entry_list, indent=4))
