import requests
import json
from locations2countries import fix_defunct_codes
import image_search

url = "http://all-olympian.eba-jjm7tanm.us-west-2.elasticbeanstalk.com/api"

if __name__ == "__main__" :

    events = requests.get(url + "/events").json()
    event_list = []
    images = {}
    with open("event_images.json", "r") as event_images :
        data = json.load(event_images)
        for entry in data :
            name = entry["event"] + " " + entry["overarching_sport"]
            images[name] = entry["image"]
    for event in events :
        top_code = event["top_athlete_country"]
        event["top_athlete_country"] = fix_defunct_codes(top_code)
		
        name = event["event"] + " " + event["overarching_sport"]
        event["image"] = images[name]
        event_list.append(event)
    print(json.dumps(event_list, indent=4))
