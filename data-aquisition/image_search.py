import requests

url = "https://api.cognitive.microsoft.com/bing/v5.0/images/search"
key = "1ea4ef1f6c6d4621a8ad9e6a0a151dee"

def search(name, count=10) :
    params = {"q": name, "count": count}
    params["offset"] = 0
    params["mkt"] = "en-us"
    params["safeSearch"] = "Moderate"

    headers = {"Ocp-Apim-Subscription-Key": key}
    response = requests.get(url, params, headers=headers)

    return [response.json()["value"][i]["thumbnailUrl"] for i in range(count)]
