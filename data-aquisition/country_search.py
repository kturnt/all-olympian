import requests
import json
import get_athletes
import code_convert

worldbank_url = "http://api.worldbank.org/v2/country"
github_url = "https://raw.githubusercontent.com/iancoleman"
# download cia factbook
factbook = github_url + "/cia_world_factbook_api/master/data/factbook.json"
facts = requests.get(factbook).json()

def main_language(language) :
    """
    language - language entry within "languages" in a factbook country entry
    returns whether to list this language in output (is it a main language?)
    main languages are official / spoken by at least 50% according to factbook
    """
    try :
        return "official" in language["note"]
    except KeyError :
        pass

    try :
        return language["percent"] >= 50
    except KeyError :
        pass

    return False

def factbook_name(name) :
    """
    name - country name collected from worldbank
    returns the country's name in factbook
    """
    name = name.lower().replace(" ", "_").replace("-", "_").replace(",", "")
    name = name.replace("st.", "saint")
    # names that are different in worldbank and factbook / special cases
    names = {
        "brunei_darussalam": "brunei",
        "congo_dem._rep.": "congo_democratic_republic_of_the",
        "congo_rep.": "congo_republic_of_the",
        "cote_d'ivoire": "cote_d'_ivoire",
        "czech_republic": "czechia",
        "egypt_arab_rep.": "egypt",
        "hong_kong_sar_china": "hong_kong",
        "iran_islamic_rep.": "iran",
        "korea_dem._people’s_rep.": "korea_north",
        "korea_rep.": "korea_south",
        "kyrgyz_republic": "kyrgyzstan",
        "lao_pdr": "laos",
        "macao_sar_china": "macau",
        "micronesia_fed._sts.": "micronesia_federated_states_of",
        "myanmar": "burma",
        "russian_federation": "russia",
        "slovak_republic": "slovakia",
        "syrian_arab_republic": "syria",
        "venezuela_rb": "venezuela",
        "virgin_islands_(u.s.)": "virgin_islands",
        "yemen_rep.": "yemen"
    }
    try :
        name = names[name]
    except KeyError :
        pass

    return name

def get_display_name(name) :
    """
    name - country name from factbook
    returns name to display on site (has correct spacing and capitalization
    """
    # names that need to be rearranged / special cases
    names = {
        "congo_democratic_republic_of_the": "Democratic Republic of the Congo",
        "congo_republic_of_the": "Republic of the Congo",
        "cote_d'_ivoire": "Cote d'Ivoire",
        "czechia": "Czech Republic",
        "korea_north": "North Korea",
        "korea_south": "South Korea",
        "micronesia_federated_states_of": "Federated States of Micronesia",
    }
    try :
        name = names[name]
    except KeyError :
        name = name.replace("_", " ").title()

    return name

def search(abbr, idno, athletes=10) :
    """
    abbr - lowercase ISO 2 code for country
    idno - ID number to assign to country
    athletes - max number of top athletes to list (default 10)
    returns dict of country data pulled from worldbank, cia factbook, and
    athlete database
    """
    response = requests.get(worldbank_url + "/" + abbr, {"format": "json"})
    country = response.json()[1][0]
    fact_name = factbook_name(country["name"])
    display_name = get_display_name(fact_name)
    country_data = {"name": fact_name, "display name": display_name}
    # convert ISO 3 code from worldbank into IOC (olympics) code
    country_data["code"] = code_convert.get_ioc(country["id"])
    country_data["id"] = idno
    country_data["capital"] = country["capitalCity"]
    country_data["region"] = country["region"]["value"].strip()
    country_data["coordinates"] = country["longitude"], country["latitude"]

    data = facts["countries"][fact_name]["data"]
    country_data["population"] = data["people"]["population"]["total"]

    gdp = data["economy"]["gdp"]["purchasing_power_parity"]["annual_values"]
    country_data["gdp"] = round(gdp[0]["value"])

    languages = data["people"]["languages"]["language"]
    # choose which languages to list in country_data
    lan_list = [lan["name"] for lan in languages if main_language(lan)]
    # just add the first language listed if no main languages identified
    if len(lan_list) == 0 :
        lan_list = [languages[0]["name"]]
    lan_list = [lan.replace(" only", "") for lan in lan_list]
    country_data["languages"] = lan_list
    country_data["flag"] = "https://www.countryflags.io/" + abbr + "/shiny/64.png"
    # collect athlete and medal data
    get_athletes.fill_athletes(country_data, athletes)

    return country_data

