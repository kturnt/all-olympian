from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd
from get_events_script import get_events

summer_sports = ["archery", "artistic-swimming", "athletics", "badminton", "baseball-softball", "basketball", "beach-volleyball", "boxing", "canoe-slalom", "canoe-sprint", "cycling-bmx", "cycling-mountain-bike", "cycling-road", "cycling-track", "diving", "equestrian-/-dressage", "equestrian-eventing", "equestrian-jumping", "fencing", "football", "golf", "gymnastics-artistic", "gymnastics-rhythmic", "handball", "hockey", "judo", "karate", "marathon-swimming", "modern-pentathlon", "rowing", "rugby", "sailing", "shooting", "skateboarding", "sport-climbing", "surfing", "swimming", "table-tennis", "taekwondo", "tennis", "trampoline", "triathlon", "volleyball", "water-polo", "weightlifting", "wrestling-freestyle", "wrestling-greco-roman"]
winter_sports = ["alpine-skiing", "biathlon", "bobsleigh", "cross-country-skiing", "curling", "figure-skating", "freestyle-skiing", "ice-hockey", "luge", "nordic-combined", "short-track", "skeleton", "ski-jumping", "snowboard", "speed-skating"]



driver = webdriver.Chrome('/usr/local/bin/chromedriver')

events = []

for sport in summer_sports:
    the_events = get_events(sport)
    sport_name = sport.capitalize()
    for event in the_events:
        driver.get("https://www.olympic.org" + event)
        content = driver.page_source
        soup = BeautifulSoup(content, "html.parser")

        # events' gender
        title = soup.title.get_text().lower().capitalize()
        try:
            event_name = soup.find("div", id="wrapper").find("section", class_="banner-box nav-sports").find("div", class_="holder").find("h1").get_text().strip()
        except AttributeError:
            event_name = title
        event_name = " ".join(w.capitalize() for w in event_name.split())
        gender = "Women" if "women" in title or "ladies" in title else "Men"

        # top athlete data 
        try:
            mydivs = soup.find("ul", attrs={"class": "related-list"}).find("li", recursive=False).findAll("a")
            top_athlete_name = (mydivs[1].find("strong", class_="name").get_text())
            top_athlete_country = (mydivs[2].find("div", class_="profile-row", recursive=False).get_text().strip())
        except AttributeError:
            top_athlete_name = "N/A"
            top_athlete_country = "N/A"
            
        # recent winner data
        try:
            past_columns = soup.find("div", class_="add-cols").findAll("div", class_="col")
            recent_winner = past_columns[0].find("table", class_="table3 alt").find("div", class_="text-box").find("strong", class_= "name").get_text().strip()
            recent_country = past_columns[0].find("table", class_="table3 alt").find("div", class_="text-box").find("div",   class_="profile-row", recursive=False).get_text().strip()
        except AttributeError:
            try:
                past_columns = soup.find("div", class_="add-cols").findAll("div", class_="col")
                recent_winner = past_columns[0].find("table", class_="table3 alt").find("div", class_="text-box").find("strong", class_= "name").get_text().strip()
                recent_country = recent_winner
            except AttributeError:
                past_columns = "N/A"
                recent_winner = "N/A"
                recent_country = "N/A"

        event_data = []
        event_data.append(event_name)
        event_data.append("Summer")
        event_data.append(sport_name)
        event_data.append(gender)
        event_data.append(top_athlete_name)
        event_data.append(top_athlete_country)
        event_data.append(recent_winner)
        event_data.append(recent_country)
        events.append(event_data)

for sport in winter_sports:
    the_events = get_events(sport)
    sport_name = sport.capitalize()
    for event in the_events:
        driver.get("https://www.olympic.org" + event)
        content = driver.page_source
        soup = BeautifulSoup(content, "html.parser")

        # events' gender
        title = soup.title.get_text().lower().capitalize()
        try:
            event_name = soup.find("div", id="wrapper").find("section", class_="banner-box nav-sports").find("div", class_="holder").find("h1").get_text().strip()
        except AttributeError:
            event_name = title
        event_name = " ".join(w.capitalize() for w in event_name.split())
        gender = "Women" if "women" in title or "ladies" in title else "Men"

        # top athlete data 
        try:
            mydivs = soup.find("ul", attrs={"class": "related-list"}).find("li", recursive=False).findAll("a")
            top_athlete_name = (mydivs[1].find("strong", class_="name").get_text())
            top_athlete_country = (mydivs[2].find("div", class_="profile-row", recursive=False).get_text().strip())
        except AttributeError:
            top_athlete_name = "N/A"
            top_athlete_country = "N/A"
        
        # recent winner data
        try:
            past_columns = soup.find("div", class_="add-cols").findAll("div", class_="col")
            recent_winner = past_columns[0].find("table", class_="table3 alt").find("div", class_="text-box").find("strong", class_= "name").get_text().strip()
            recent_country = past_columns[0].find("table", class_="table3 alt").find("div", class_="text-box").find("div",   class_="profile-row", recursive=False).get_text().strip()
        except AttributeError:
            try:
                past_columns = soup.find("div", class_="add-cols").findAll("div", class_="col")
                recent_winner = past_columns[0].find("table", class_="table3 alt").find("div", class_="text-box").find("strong", class_= "name").get_text().strip()
                recent_country = recent_winner
            except AttributeError:
                past_columns = "N/A"
                recent_winner = "N/A"
                recent_country = "N/A"

        event_data = []
        event_data.append(event_name)
        event_data.append("Winter")
        event_data.append(sport_name)
        event_data.append(gender)
        event_data.append(top_athlete_name)
        event_data.append(top_athlete_country)
        event_data.append(recent_winner)
        event_data.append(recent_country)
        events.append(event_data)



driver.quit()
#print(events)

df = pd.DataFrame(events, columns= ['Event', 'Season', 'Sport', 'Gender', 'Top-Athlete-Name', 'Top-Athlete-Country', 'Most-Recent-Champion-Name', 'Most-Recent-Champion-Country'])
df.to_csv('events.csv', index=False, encoding='utf-8')