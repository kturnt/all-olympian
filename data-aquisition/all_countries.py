import requests
import json
import country_search

if __name__ == "__main__" :
    # get all countries from worldbank
    params = {"format": "json", "per_page": 304}
    response = requests.get(country_search.worldbank_url, params).json()
    countries = []
    idno = 1
    for country in response[1] :
        if country["longitude"] != "" :
            countries.append(country_search.search(country["iso2Code"].lower(), idno))
            idno += 1
    print(json.dumps(countries, indent=4))
