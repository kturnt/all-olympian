import requests
import json

def convert_name(name) :
    names = {
        "Great Britain": "United Kingdom",
        "United States of America": "United States",
        "People's Republic of China": "China",
        "Russian Federation": "Russia",
        "Yugoslavia": "Bosnia And Herzegovina",
        "USSR": "Russia",
        "Republic of Korea": "South Korea"
    }

    try :
        name = names[name]
    except KeyError :
        pass

    return name

def fix_defunct_codes(code) :
    if code == "GDR" :
        code = "GER"
    elif code == "URS" :
        code = "RUS"

    return code

def main() :
    locations_url = "http://all-olympian.eba-jjm7tanm.us-west-2.elasticbeanstalk.com/api/olympics"
    locations = requests.get(locations_url).json()
    location_list = []
    codes = {}
    with open("countries.json", "r") as countries :
        data = json.load(countries)
        codes = {country["display name"]: country["code"] for country in data}
    for location in locations :
        display_name = convert_name(location["hosting_country"])
        location["hosting_country"] = codes[display_name]
        code = fix_defunct_codes(location["top_athlete_country"])
        location["top_athlete_country"] = code
        location_list.append(location)
    print(json.dumps(location_list, indent=4))


if __name__ == "__main__" :
    main()
    

