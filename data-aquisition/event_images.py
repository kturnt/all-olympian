import csv
import json
import image_search

if __name__ == "__main__" :
    csv_file = csv.reader(open("events.csv", "r"), delimiter=",")
    entry_list = []
    read_file = iter(csv_file)
    next(read_file)
    for line in read_file :
        entry = {"event": line[0], "overarching_sport": line[2]}
        query = "olympics " + line[2] + " " + line[0]
        entry["image"] = image_search.search(query, 1)[0]
        entry_list.append(entry)
    print(json.dumps(entry_list, indent=4))
