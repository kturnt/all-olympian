import json

if __name__ == "__main__" :
    hosts = {}
    with open("locations.json", "r") as olympics :
        data = json.load(olympics)
        for location in data :
            code = location["hosting_country"]
            if code not in hosts :
                hosts[code] = []
            hosts[code].append(location["id"])

    all_time_bests = {}
    recent_wins = {}
    with open("events.json", "r") as events :
        data = json.load(events)
        for event in data :
            code = event["top_athlete_country"]
            if code not in all_time_bests :
                all_time_bests[code] = []
            all_time_bests[code].append(event["id"])
            code = event["latest_champion_country"]
            if code not in recent_wins :
                recent_wins[code] = []
            recent_wins[code].append(event["id"])

    country_list = []
    with open("countries.json", "r") as countries :
        data = json.load(countries)
        for country in data :
            code = country["code"]
            try :
                country["hosted"] = hosts[code]
            except KeyError :
                country["hosted"] = []
            try :
                country["all_time_best_events"] = all_time_bests[code]
            except KeyError :
                country["all_time_best_events"] = []
            try :
                country["recent_wins"] = recent_wins[code]
            except KeyError :
                country["recent_wins"] = []
            country_list.append(country)
    print(json.dumps(country_list, indent=4))

