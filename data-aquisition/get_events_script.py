from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd

def get_events(sport):
    urls = []
    driver = webdriver.Chrome('/usr/local/bin/chromedriver')

    driver.get("https://www.olympic.org/" + sport)
    content = driver.page_source
    soup = BeautifulSoup(content, "html.parser")
    
    list_rows = soup.findAll("div", class_="list-row")
    for grouping in list_rows:
        uls = grouping.findAll("a", href=True)
        newurls = [ul["href"] for ul in uls]
        urls += newurls

    driver.close()

    return urls

