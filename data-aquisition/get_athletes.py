import csv
import re

def fill_athletes(country_data, count=10) :
    """
    counrty_data - dict of country data from country search
    count - number of entries for top athletes to add, default top 10
    adds medal count, medals, and athlete entries from athlete_events.csv
    """
    csv_file = csv.reader(open("athlete_events.csv", "r"), delimiter=",")
    athlete_map = {}
    medal_map = {}
    participation = set()
    medal_count = 0
    for entry in csv_file :
        team = entry[7]
        medal = entry[-1]
        if team == country_data["code"] : # athlete's noc
            if medal != "NA" :
                name = re.sub(" *\(.*\)", "", entry[1]) # delete parentheses from name
                sport = entry[-3]
                if name not in athlete_map :
                    athlete_map[name] = dict()
                if sport not in athlete_map[name] :
                    athlete_map[name][sport] = {"Gold": 0, "Silver": 0, "Bronze": 0}
                if sport not in medal_map :
                    medal_map[sport] = {"Gold": 0, "Silver": 0, "Bronze": 0}
                # count medal towards total, total for sport, and total for athlete
                medals = athlete_map[name][sport][medal] + 1
                athlete_map[name][sport][medal] = medals
                medals = medal_map[sport][medal] + 1
                medal_map[sport][medal] = medals
                medal_count += 1
            participation.add(entry[10] + " " + entry[9] + ", " + entry[11])

    country_data["attended"] = list(participation)

    def location_sorter(location) :
        parse_location = location.split()
        val = int(parse_location[1][:-1])
        if parse_location[0] == "Summer" :
            val += 1

        return val

    country_data["attended"].sort(key=location_sorter)
    country_data["medal count"] = medal_count
    country_data["medals"] = medal_map

    # filter out all but top athletes (most medals won)
    athlete_list = list(athlete_map.items())

    def athlete_sorter(athlete) :
        # return athlete's total number of medals
        return sum(sum(athlete[1][sport].values()) for sport in athlete[1])

    athlete_list.sort(reverse=True, key=athlete_sorter)

    new_map = dict()
    new_map.update(athlete_list[:count])
    
    country_data["athletes"] = new_map

