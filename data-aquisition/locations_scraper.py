from selenium import webdriver
from bs4 import BeautifulSoup
import pandas as pd
from get_locations_script import get_locations

urls = get_locations()
locations = []
all_locations_data = []

driver = webdriver.Chrome('/usr/local/bin/chromedriver')

for url in urls:
    driver.get("https://www.olympic.org" + url)
    content = driver.page_source
    soup = BeautifulSoup(content, "html.parser")
    
    curr_location_data = []
    
    # City, Year
    city_data = soup.find("div", id="wrapper").find("section", class_="profiles alt2").find("div", class_="holder").find("div", class_="text-box").get_text().strip().split()
    city_name = ""
    for i in range(0, len(city_data)-1):
        if i != len(city_data) - 2:
            city_name += city_data[i] + " "
        else:
            city_name += city_data[i]
    year = city_data[len(city_data)-1]
    curr_location_data.append(city_name)
    curr_location_data.append(year)

    # Season
    season_data = soup.find("title").get_text().strip()
    season = "Winter" if "Winter" in season_data else "Summer"
    curr_location_data.append(season)

    # Games Stats
    info_banner_list = soup.find("div", id="wrapper").find("div", class_="frame").find("ul").findAll("div", class_="text-box")

    hosting_country = info_banner_list[1].find("a").get_text().strip()
    num_countries = info_banner_list[3].get_text().strip()
    countries = ''.join(filter(str.isdigit, num_countries))
    num_athletes = info_banner_list[2].get_text().strip()
    athletes = ''.join(filter(str.isdigit, num_athletes))
    num_events = info_banner_list[4].get_text().strip()
    events = ''.join(filter(str.isdigit, num_events))

    # appended in order of country, number of countries, number of athletes, number of events
    curr_location_data.append(hosting_country)
    curr_location_data.append(countries)
    curr_location_data.append(athletes)
    curr_location_data.append(events)

    # Top Athlete, Top Athlete's Country
    try:
        top_athlete_data = soup.find("section", class_="results ajax-area no-history").find("ul", class_= "table2 ajax-content").findAll("li")[1]
        top_athlete_name = top_athlete_data.find("div", class_="td col1").find("strong", class_="name").get_text().strip()
        top_athlete_country = top_athlete_data.find("div", class_="td col-first").find("div", class_="profile-row").get_text().strip()      
    except AttributeError:
        try:
            top_athlete_data = soup.find("section", class_="results ajax-area").find("ul", class_= "table2 ajax-content").findAll("li")[1]
            top_athlete_name = top_athlete_data.find("div", class_="td col1").find("strong", class_="name").get_text().strip()
            top_athlete_country = top_athlete_data.find("div", class_="td col-first").find("div", class_="profile-row").get_text().strip()        
        except AttributeError:
            top_athlete_name = "N/A"
            top_athlete_country = "N/A"
    curr_location_data.append(top_athlete_name)
    curr_location_data.append(top_athlete_country)

    # Add to master list
    all_locations_data.append(curr_location_data)

driver.quit()

df = pd.DataFrame(all_locations_data, columns= ['City', 'Year', 'Season', 'Host Country', 'Number of Countries', 'Number of Athletes', 'Number of Events', 'Top Athlete Name', 'Top Athlete Country'])
df.to_csv('locations.csv', index=False, encoding='utf-8')

    

    


    
