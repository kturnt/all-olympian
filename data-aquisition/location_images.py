import csv
import json
import image_search

if __name__ == "__main__" :
    csv_file = csv.reader(open("locations.csv", "r"), delimiter=",")
    entry_list = []
    read_file = iter(csv_file)
    next(read_file)
    for line in read_file :
        entry = {"location": line[0], "year": line[1]}
        query = line[0] + " " + line[1] + " olympics logo"
        entry["logo"] = image_search.search(query, 1)[0]
        query = line[0] + " " + line[1] + " olympics opening ceremony"
        entry["opening ceremony"] = image_search.search(query, 1)[0]
        entry_list.append(entry)
    print(json.dumps(entry_list, indent=4))
