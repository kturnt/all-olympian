CS 373 11 am Group 10 Project

Name:               EID:        GitLab ID             Estimate        Actual
Kevin Turner        kt24387     kturnt                30hr            25hr
Max Patrick         mep3343     maxpat                30hr            25hr
Ryan Resma          rmr3429     rresma99              30hr            25hr
Laith Alsukhni      la23936     laith_alsukhni        30hr            25hr
Campbell Sinclair   cds3995     csinclair             30hr            25hr

git sha: 7a6c7929c1c35426d4fec5973ace9bf4b5910206

Project Leader: Kevin Turner

Gitlab Pipelines: https://gitlab.com/kturnt/all-olympian/pipelines

Website Link: https://www.all-olympian.com/

Estimated completion time: 30 hours

Actual completion time: 25 hours


